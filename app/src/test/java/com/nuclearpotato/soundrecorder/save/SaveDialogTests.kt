package com.nuclearpotato.soundrecorder.save

import com.nuclearpotato.soundrecorder.BuildConfig
import com.nuclearpotato.soundrecorder.R
import com.nuclearpotato.soundrecorder.model.Duration
import com.nuclearpotato.soundrecorder.model.Recording
import com.nuclearpotato.soundrecorder.model.RecordingStoreStub
import com.nuclearpotato.soundrecorder.util.TestApplication
import org.hamcrest.CoreMatchers.`is`
import org.junit.Assert.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricGradleTestRunner
import org.robolectric.RuntimeEnvironment
import org.robolectric.annotation.Config
import rx.Observable
import rx.subjects.PublishSubject

@RunWith(RobolectricGradleTestRunner::class)
@Config(
        constants = BuildConfig::class,
        sdk = intArrayOf(21),
        application = TestApplication::class)
class SaveDialogTests {

    private val context = RuntimeEnvironment.application
    private val store = RecordingStoreStub()
    private val view = SaveDialogViewStub()

    @Test
    fun testDialogSetup() {
        val recording = store.add(Recording("foo", 1000))
        SaveDialogPresenter(context, store, recording, view)
        assertThat(view.name, `is`("foo"))
        assertThat(view.durationField, `is`(Duration(1000)))
    }

    @Test
    fun testSave() {
        val recording = store.add(Recording("foo", 1000))
        val presenter = SaveDialogPresenter(context, store, recording, view)
        view.name = "bar"
        presenter.onSaveClick()
        assertThat(store.recordings["bar"], `is`(Recording("bar", 1000)))
    }

    @Test
    fun testDelete() {
        val recording = store.add(Recording("foo", 1000))
        val presenter = SaveDialogPresenter(context, store, recording, view)
        presenter.onDelete()
        assertThat(store.recordings.size, `is`(0))
    }

    @Test
    fun testIllegalChar() {
        val recording = store.add(Recording("foo", 1000))
        val presenter = SaveDialogPresenter(context, store, recording, view)
        view.name = "/"
        assertThat(view.errorField, `is`(context.getString(R.string.dialog_error_illegal, '/')))
        assertThat(view.buttonEnabled, `is`(false))
        view.name = "a"
        assertThat(view.errorField, `is`(""))
        assertThat(view.buttonEnabled, `is`(true))
    }

    class SaveDialogViewStub : SaveDialogView {

        var durationField: Duration? = null
        var errorField: String? = null
        var buttonEnabled: Boolean = false
        val nameSubject = PublishSubject.create<String>()
        override fun setDuration(duration: Duration) {
            durationField = duration
        }

        override var name: String = ""
            set(value) {
                field = value
                nameSubject.onNext(value)
            }

        override fun setError(error: String?) {
            errorField = error
        }

        override fun enablePositiveButton(enable: Boolean) {
            buttonEnabled = enable
        }

        override fun nameChanges(): Observable<String> {
            return nameSubject
        }
    }
}
