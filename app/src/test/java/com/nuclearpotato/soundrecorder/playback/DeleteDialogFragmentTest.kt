package com.nuclearpotato.soundrecorder.playback

import com.nuclearpotato.soundrecorder.model.Recording
import org.hamcrest.Matchers
import org.junit.Assert.*
import org.junit.Test

class DeleteDialogFragmentTest {

    val recording1 = Recording("foo", null)
    val recording2 = Recording("bar", null)

    @Test
    fun SingleRecording() {
        test(listOf(recording1), "foo")
    }

    @Test
    fun MultipleRecordings() {
        test(listOf(recording1, recording2), "1. foo\n2. bar")
    }

    @Test
    fun EmptyOnEmptyList() {
        test(listOf(), "")
    }

    private fun test(recordings: List<Recording>, expected: String) {
        val result = DeleteDialogFragment.generateBody(recordings)
        assertThat(result, Matchers.`is`(Matchers.equalTo(expected)))
    }
}