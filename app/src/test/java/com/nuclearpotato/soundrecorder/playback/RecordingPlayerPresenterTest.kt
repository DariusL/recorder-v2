package com.nuclearpotato.soundrecorder.playback

import com.nhaarman.mockito_kotlin.never
import com.nhaarman.mockito_kotlin.spy
import com.nhaarman.mockito_kotlin.verify
import com.nuclearpotato.soundrecorder.model.Duration
import com.nuclearpotato.soundrecorder.model.Recording
import com.nuclearpotato.soundrecorder.model.RecordingStore
import com.nuclearpotato.soundrecorder.model.RecordingStoreStub
import com.nuclearpotato.soundrecorder.playback.RecordingListPresenter
import com.nuclearpotato.soundrecorder.playback.RecordingListView
import com.nuclearpotato.soundrecorder.playback.RecordingReader
import com.nuclearpotato.soundrecorder.playback.RecordingViewModel
import com.nuclearpotato.soundrecorder.util.argThat
import com.nuclearpotato.soundrecorder.util.propertyMatches
import org.hamcrest.Matcher
import org.hamcrest.Matchers.*
import org.hamcrest.core.IsCollectionContaining
import org.junit.Assert.assertThat
import org.junit.Test
import org.mockito.Matchers
import rx.Scheduler
import rx.schedulers.Schedulers
import rx.schedulers.TestScheduler
import java.util.*

class RecordingPlayerPresenterTest {

    val recordingsStore = RecordingStoreStub()
    val view = spy(RecordingListViewMock())

    val recordingsReader = RecordingReaderMock()

    @Test
    fun testBind() {
        addRecording(Recording("foo", null))
        instantiate()
        assertThat(view.recordingsField, hasItems(recordingIs(recordingWithLength("foo"))))
        assertThat(view.recordingsField, not(contains(isPlaying(true))))
        assertThat(view.recordingsField, not(contains(isSelected(true))))
    }

    @Test
    fun testPlayOnRecordingPress() {
        addRecording(Recording("foo", null))
        val presenter = instantiate()
        presenter.onRecordingPress(0)
        assertThat(view.player.recording, `is`(recordingWithLength("foo")))
        assertThat(view.recordingsField, hasItems(isPlaying(true)))
    }

    @Test
    fun testCurrentRecordingChanges() {
        addRecording(Recording("foo", null))
        addRecording(Recording("bar", null))
        val presenter = instantiate()
        presenter.onRecordingPress(0)
        presenter.onRecordingPress(1)
        assertThat(view.recordingsField, hasItems(isPlaying(false), isPlaying(true)))
    }

    @Test
    fun testShowLoading() {
        val scheduler = TestScheduler()
        instantiate(workerScheduler = scheduler)
        assertThat(view.loadingField, `is`(true))
        scheduler.triggerActions()
        assertThat(view.loadingField, `is`(false))
    }

    @Test
    fun testEnableActionModeWhenSelectionIsPressed() {
        val presenter = instantiate()
        presenter.onSelectionPress()
        assertThat(view.actionMode, `is`(not(nullValue())))
        assertThat(view.actionMode!!.count, `is`(0))
    }

    @Test
    fun testPressedItemsWhenSelectingAreSelected() {
        addRecording(Recording("foo", null))
        addRecording(Recording("bar", null))
        addRecording(Recording("baz", null))
        val presenter = instantiate()
        presenter.onSelectionPress()
        presenter.onRecordingPress(1)
        assertThat(view.recordingsField[1], isSelected(true))
        assertThat(view.actionMode!!.count, `is`(1))
        assertThat(view.player.recording, `is`(nullValue()))
    }

    @Test
    fun testSelectMultipleRecordings() {
        addRecording(Recording("foo", null))
        addRecording(Recording("bar", null))
        addRecording(Recording("baz", null))
        val presenter = instantiate()
        presenter.onSelectionPress()
        presenter.onRecordingPress(1)
        presenter.onRecordingPress(2)
        assertThat(view.recordingsField[1], isSelected(true))
        assertThat(view.recordingsField[2], isSelected(true))
        assertThat(view.actionMode!!.count, `is`(2))
        assertThat(view.player.recording, `is`(nullValue()))
    }

    @Test
    fun testUnselectPressedRecording() {
        addRecording(Recording("foo", null))
        addRecording(Recording("bar", null))
        addRecording(Recording("baz", null))
        val presenter = instantiate()
        presenter.onSelectionPress()
        presenter.onRecordingPress(1)
        presenter.onRecordingPress(2)
        presenter.onRecordingPress(1)
        assertThat(view.recordingsField[1], isSelected(false))
        assertThat(view.actionMode!!.count, `is`(1))
        assertThat(view.player.recording, `is`(nullValue()))
    }

    @Test
    fun testDisableActionModeWhenAllItemsAreUnselected() {
        addRecording(Recording("foo", null))
        val presenter = instantiate()
        presenter.onSelectionPress()
        presenter.onRecordingPress(0)
        presenter.onRecordingPress(0)
        assertThat(view.actionMode, `is`(nullValue()))
    }

    @Test
    fun testNoDisableCallOnDestroy() {
        addRecording(Recording("foo", null))
        val presenter = instantiate()
        presenter.onSelectionPress()
        presenter.onActionModeDestroyed()
        assertThat(view.actionMode, `is`(notNullValue()))
    }

    @Test
    fun testSelectRecordingOnLongPress() {
        addRecording(Recording("foo", null))
        val presenter = instantiate()
        presenter.onRecordingLongPress(0)
        assertThat(view.recordingsField[0], isSelected(true))
    }

    @Test(expected = IllegalStateException::class)
    fun testExceptionWhenDeleteOutOfSelection() {
        instantiate().onDeletePress()
    }

    @Test(expected = IllegalStateException::class)
    fun testExceptionWhenDeletingWithNoItems() {
        instantiate().apply {
            onSelectionPress()
            onDeletePress()
        }
    }

    @Test
    fun testDeleteRecordings() {
        add(recording("foo"), recording("bar"), recording("baz"))
        val presenter = instantiate()
        presenter.select(1, 2)
        presenter.onDeletePress()
        verify(view).showDeleteDialog(argThat(hasItems(recordingNameIs("bar"), recordingNameIs("baz"))))
    }

    @Test
    fun testDeleteConfirm() {
        add(recording("foo"), recording("bar"), recording("baz"))
        val presenter = instantiate()
        presenter.select(1, 2)
        presenter.onDeleteConfirm()
        assertThat(view.recordingsField, `is`(hasItems(recordingIs(recordingWithLength("foo")))))
        assertThat(recordingsStore.listRecordings(), hasItems(recordingNameIs("foo")))
    }

    @Test
    fun testDeleteClosesActionMode() {
        addRecording(recording("foo"))
        val presenter = instantiate()
        presenter.select(0)
        presenter.onDeleteConfirm()
        assertThat(view.actionMode, `is`(nullValue()))
    }

    @Test
    fun testPressRenamePress() {
        addRecording(recording("foo"))
        val presenter = instantiate()
        presenter.select(0)
        presenter.onRenamePress()
        verify(view).showRenameDialog(recordingWithLength("foo"));
    }

    @Test
    fun testRenameConfirm() {
        add(recording("foo"), recording("baz"))
        val presenter = instantiate()
        presenter.select(0)
        presenter.onRenameConfirm("bar")

        assertThat(recordingsStore.listRecordings(), hasItems(recordingNameIs("bar")))
        assertThat(view.recordingsField, hasItems(recordingIs(recordingWithLength("bar"))))
    }

    @Test
    fun testSelectionIsClearedOnActionModeDestroy() {
        add(recording("foo"))
        val presenter = instantiate()
        presenter.select(0)
        presenter.onActionModeDestroyed()
        assertThat(view.recordingsField, not(contains(isSelected(true))))
    }

    @Test
    fun testStopPlayingRecordingIfDeleted() {
        add(recording("foo"), recording("baz"))
        val presenter = instantiate()
        presenter.onRecordingPress(0)
        presenter.onRecordingLongPress(0)
        presenter.onDeleteConfirm()

        assertThat(view.player.recording, `is`(nullValue()))
    }

    @Test
    fun testContinuePlayingIfDeletingNotPlayingRecording() {
        add(recording("foo"), recording("baz"))
        val presenter = instantiate()
        presenter.onRecordingPress(0)
        presenter.onRecordingLongPress(1)
        presenter.onDeleteConfirm()

        assertThat(view.player.recording, `is`(recordingWithLength("foo")))
    }

    @Test
    fun testSelectAll() {
        add(recording("foo"), recording("baz"))
        val presenter = instantiate()
        presenter.onSelectionPress()
        presenter.onSelectAllPress()
        assertThat(view.recordingsField, everyItem(isSelected(true)))
        assertThat(view.actionMode!!.count, `is`(2))
    }

    private fun RecordingListPresenter.select(vararg items: Int) {
        onSelectionPress()
        items.forEach { onRecordingPress(it) }
    }

    private fun add(vararg recordings: Recording) {
        recordings.forEach { addRecording(it) }
    }

    private fun addRecording(recording: Recording) {
        recordingsStore.add(recording)
    }

    private fun isPlaying(playing: Boolean): Matcher<RecordingViewModel> {
        return propertyMatches(`is`(playing), RecordingViewModel::playing)
    }

    private fun isSelected(selected: Boolean): Matcher<RecordingViewModel> {
        return propertyMatches(`is`(selected), RecordingViewModel::selected)
    }

    private fun recordingNameIs(name: String): Matcher<Recording> {
        return propertyMatches(`is`(name), Recording::name)
    }

    private fun recordingIs(recording: Recording): Matcher<RecordingViewModel> = recordingMatches(`is`(recording))

    private fun recordingMatches(matcher: Matcher<Recording>): Matcher<RecordingViewModel> {
        return propertyMatches(matcher, RecordingViewModel::recording)
    }

    private fun recording(name: String) = Recording(name, null)

    private fun recordingWithLength(name: String): Recording {
        return Recording(name, name.length.toLong())
    }

    private fun RecordingPlayerPresenterTest.instantiate(
            recordingsStore: RecordingStore = this.recordingsStore,
            recordingsReader: RecordingReader = this.recordingsReader,
            workerScheduler: Scheduler = Schedulers.immediate()!!,
            mainThreadScheduler: Scheduler = Schedulers.immediate()!!): RecordingListPresenter {
        val presenter = RecordingListPresenter(recordingsStore, recordingsReader, workerScheduler, mainThreadScheduler, view)
        view.presenter = presenter
        return presenter
    }

    open class RecordingListViewMock : RecordingListView {

        lateinit var presenter: RecordingListPresenter

        var loadingField: Boolean? = false
        override val player = MockPlayer()
        lateinit var recordingsField: List<RecordingViewModel>
        var actionMode: MockActionMode? = null

        override fun setRecordings(recordings: List<RecordingViewModel>) {
            recordingsField = ArrayList(recordings)
        }

        override fun enableActionMode(): RecordingListView.ActionMode {
            actionMode = MockActionMode()
            return actionMode!!
        }

        override fun showDeleteDialog(recordings: List<Recording>) {
        }

        override fun showRenameDialog(recording: Recording) {
        }

        override fun setLoading(loading: Boolean) {
            loadingField = loading
        }

        inner class MockActionMode : RecordingListView.ActionMode {
            override var count = 0

            override fun disable() {
                actionMode = null
                presenter.onActionModeDestroyed()
            }
        }
    }

    class MockPlayer : RecordingListView.Player {
        var recording: Recording? = null

        override fun play(recording: Recording) {
            this.recording = recording
        }

        override fun stop() {
            recording = null
        }
    }

    class RecordingReaderMock : RecordingReader {
        override fun read(recording: Recording): Recording {
            return recording.copy(duration = Duration(recording.name.length.toLong()))
        }
    }
}