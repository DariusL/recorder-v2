package com.nuclearpotato.soundrecorder.recording

import com.nhaarman.mockito_kotlin.*
import com.nuclearpotato.soundrecorder.model.Recording
import org.hamcrest.CoreMatchers.*
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.junit.MockitoJUnit
import rx.schedulers.Schedulers
import rx.schedulers.TestScheduler
import java.util.concurrent.TimeUnit

class RecordingPresenterTest {

    private val someRecording = Recording("foo", 1000)

    @Suppress("unused")
    @get:Rule val mockitoRule = MockitoJUnit.rule()
    val connection = mock<RecorderConnection>()
    var callbacksField: RecorderConnection.Callbacks? = null
    val view = spy(RecordingViewMock())
    val scheduler = TestScheduler();
    val presenter = RecordingPresenter(connection, view, {currentTime}, scheduler, Schedulers.immediate())

    var currentTime: Long = 0
        set(value) {
            field = value
            scheduler.advanceTimeTo(value, TimeUnit.MILLISECONDS)
        }

    val callbacks: RecorderConnection.Callbacks
        get() = callbacksField!!

    @Before
    fun setUp() {
        doAnswer { invocation ->
            callbacksField = invocation.arguments[0] as RecorderConnection.Callbacks
        }.whenever(connection).connect(any())

        doAnswer { invocation ->
            callbacksField = null
        }.whenever(connection).disconnect()
        currentTime = 0
    }

    @Test
    fun testConnectOnStart() {
        presenter.onStart()
        assertThat(callbacks, `is`(notNullValue()))
        assertThat(view.buttonEnabled, `is`(false))
        verify(connection).connect(callbacks)
    }

    @Test
    fun testEnableButtonOnPrepared() {
        presenter.onStart()
        callbacks.onPrepared()
        assertThat(view.buttonEnabled, `is`(true))
    }

    @Test
    fun testSendStartToService() {
        presenter.onStart()
        callbacks.onPrepared()
        presenter.onButtonClick()
        assertThat(view.buttonEnabled, `is`(false))
        verify(connection).start()
    }

    @Test
    fun testUiShowsStarted() {
        presenter.onStart()
        callbacks.onRecording(0)
        assertThat(view.buttonEnabled, `is`(true))
        assertThat(view.recordingState, `is`(true))
    }

    @Test
    fun testInitializeUiToMaybe() {
        presenter.onStart()
        assertThat(view.recordingState, nullValue())
    }

    @Test
    fun testUiStateInStart() {
        presenter.onStart()
        callbacks.onPrepared()
        assertThat(view.recordingState, `is`(false))
        assertThat(view.buttonEnabled, `is`(true))
        presenter.onButtonClick()
        assertThat(view.recordingState, `is`(false))
        assertThat(view.buttonEnabled, `is`(false))
        callbacks.onRecording(0)
        assertThat(view.recordingState, `is`(true))
        assertThat(view.buttonEnabled, `is`(true))
    }

    @Test
    fun testPresenterDisconnectsOnStop() {
        presenter.onStart()
        presenter.onStop()
        verify(connection).disconnect()
        assertThat(callbacksField, `is`(nullValue())) // testing the mock really
    }

    @Test
    fun testUiLifecycle() {
        presenter.onStart()
        callbacks.onPrepared()
        presenter.onStop()
        presenter.onStart()
        assertThat(view.buttonEnabled, `is`(false))
    }

    @Test
    fun testStop() {
        presenter.onStart()
        callbacks.onRecording(0)
        presenter.onButtonClick()
        verify(connection).stop()
        assertThat(view.buttonEnabled, `is`(false))
        assertThat(view.recordingState, `is`(true))
        callbacks.onRecordingComplete(someRecording)
        assertThat(view.buttonEnabled, `is`(true))
        assertThat(view.recordingState, `is`(false))
    }

    @Test
    fun testCallSave() {
        presenter.onStart()
        val recording = someRecording
        callbacks.onRecordingComplete(recording)
        verify(view).saveRecording(recording)
    }

    @Test
    fun testShowRecordingLength() {
        presenter.onStart()
        callbacks.onRecording(0)
        currentTime = 500
        assertThat(view.time, `is`(500L))
    }

    @Test
    fun testTimeIsUpdatedAfterPeriodPasses() {
        presenter.onStart()
        callbacks.onRecording(0)
        currentTime = 1000L
        assertThat(view.time, `is`(1000L))
        currentTime = 2000L
        assertThat(view.time, `is`(2000L))
    }

    @Test
    fun testTimeUpdatesAreNotTooFrequent() {
        presenter.onStart()
        callbacks.onRecording(0)
        currentTime = 1000L
        currentTime = 1500L
        assertThat(view.time, `is`(1000L))
    }

    @Test
    fun testTimeUpdatesStopWhenDisconnected() {
        presenter.onStart()
        callbacks.onRecording(0)
        assertThat(view.time, `is`(0L))
        presenter.onStop()
        currentTime = 1000
        assertThat(view.time, `is`(0L))
    }

    @Test
    fun testTimeUpdatesStopWhenRecordingStops() {
        presenter.onStart()
        callbacks.onRecording(0)
        callbacks.onRecordingComplete(someRecording)
        currentTime = 1000
        assertThat(view.time, `is`(0L))
    }

    open class RecordingViewMock : RecordingView {

        var buttonEnabled = true
        var recordingState: Boolean? = null
        var time = 0L

        override fun enableButton(enable: Boolean) {
            buttonEnabled = enable
        }

        override fun setRecording(recording: Boolean?) {
            recordingState = recording
        }

        override fun saveRecording(recording: Recording) {}

        override fun setRecordingTime(time: Long) {
            this.time = time;
        }
    }
}