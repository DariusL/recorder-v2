package com.nuclearpotato.soundrecorder.util;

import com.nuclearpotato.soundrecorder.Util;

import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import rx.Observable;
import rx.functions.Func2;
import rx.plugins.RxJavaHooks;
import rx.plugins.RxJavaObservableExecutionHook;
import rx.plugins.RxJavaPlugins;

public class RxSchedulerDebuggerRule implements TestRule {

    @Override
    public Statement apply(final Statement base, Description description) {
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                Hook hook = new Hook();
                try {
                    RxJavaHooks.setOnObservableStart(hook);
                    base.evaluate();
                } catch (Throwable throwable) {
                    throw new Exception("Test failed with threads " + hook.getThreads(), throwable);
                } finally {
                    RxJavaHooks.clear();
                }
            }
        };
    }

    private class Hook implements Func2<Observable, Observable.OnSubscribe, Observable.OnSubscribe> {

        private final Set<String> threads = Collections.synchronizedSet(new HashSet<String>());
        @Override
        public Observable.OnSubscribe call(Observable observable, Observable.OnSubscribe onSubscribe) {
            threads.add(Thread.currentThread().getName());
            return onSubscribe;
        }

        private String getThreads(){
            return Util.INSTANCE.join(threads, ", ");
        }
    }
}
