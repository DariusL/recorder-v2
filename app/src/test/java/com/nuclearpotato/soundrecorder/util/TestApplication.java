package com.nuclearpotato.soundrecorder.util;

import android.app.Application;

import java.util.HashMap;
import java.util.Map;

public class TestApplication extends Application {
    private final Map<String, Object> services = new HashMap<>();

    public void addSystemService(String name, Object service){
        services.put(name, service);
    }

    @Override
    public Object getSystemService(String name) {
        if(services.containsKey(name)){
            return services.get(name);
        }
        return super.getSystemService(name);
    }
}
