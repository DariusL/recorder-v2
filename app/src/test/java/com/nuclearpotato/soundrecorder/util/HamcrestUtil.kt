package com.nuclearpotato.soundrecorder.util

import com.nhaarman.mockito_kotlin.argThat
import org.hamcrest.FeatureMatcher
import org.hamcrest.Matcher
import org.mockito.ArgumentMatcher
import org.mockito.Matchers
import org.mockito.Mockito
import kotlin.reflect.KProperty

/**
 * Match property against specified matcher.
 *
 * @param matcher the matcher to run
 * @param property the property on whose value to run it.
 *
 * @param Ot type of the object to match
 * @param Pt type of the property to match
 */
fun <Ot, Pt> propertyMatches(matcher: Matcher<Pt>, property: KProperty<Pt>) : Matcher<Ot> {
    return object : FeatureMatcher<Ot, Pt>(matcher, property.name, property.name) {
        override fun featureValueOf(actual: Ot): Pt {
            return property.call(actual)
        }
    }
}

fun <T> wrap(hamcrestMatcher: Matcher<in T>): ArgumentMatcher<out T> {
    return ArgumentMatcher<T> { argument -> hamcrestMatcher.matches(argument) }
}

fun <T> argThat(hamcrestMatcher: Matcher<in T>): T {
    return Matchers.argThat(wrap(hamcrestMatcher))
}