package com.nuclearpotato.soundrecorder;

import org.junit.Ignore;
import org.junit.Test;

import rx.Observable;
import rx.functions.Func2;
import rx.plugins.RxJavaHooks;
import rx.plugins.RxJavaObservableExecutionHook;
import rx.plugins.RxJavaPlugins;

import static org.junit.Assert.assertTrue;

public class HookTest {

    @Test
    public void testChangeHooks() {
        Hook first = new Hook();
        RxJavaHooks.setOnObservableStart(first);

        Observable.just(null).subscribe();

        assertTrue(first.invoked);

        RxJavaHooks.clear();
        Hook second = new Hook();
        RxJavaHooks.setOnObservableStart(second);

        Observable.just(null).subscribe();

        assertTrue(second.invoked);
    }

    private class Hook implements Func2<Observable, Observable.OnSubscribe, Observable.OnSubscribe> {
        volatile boolean invoked = false;
        @Override
        public Observable.OnSubscribe call(Observable observable, Observable.OnSubscribe onSubscribe) {
            invoked = true;
            return onSubscribe;
        }
    }
}