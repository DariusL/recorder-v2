package com.nuclearpotato.soundrecorder.model

import org.hamcrest.CoreMatchers
import org.hamcrest.CoreMatchers.*
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TemporaryFolder
import java.io.File
import java.io.IOException

class RecordingStoreTest{
    @get:Rule val tempFolder = TemporaryFolder()

    @Before
    fun setUp() {
        tempFolder.create()
    }

    @Test
    fun testNameValidator() {
        assertNameValid("", RecordingUtils.NAME_EMPTY)
        assertNameValid("a", RecordingUtils.NAME_VALID)
        assertNameValid("/", '/')
        assertNameValid("asasgasg?", '?')
    }

    @Test
    fun testDirSetup() {
        val store = RecordingStoreImpl(tempFolder.root)
        store.create()
        assertFileExists(tempFolder.root, ".nomedia")
    }

    private fun assertFileExists(dir: File, name: String) {
        val file = File(dir, name)
        assertThat(file.exists(), `is`(true))
    }

    private fun assertNameValid(name: String, expected: Int) {
        assertThat(RecordingUtils.isNameValid(name), `is`(expected))
    }

    private fun assertNameValid(name: String, illegal: Char) {
        val flags = RecordingUtils.isNameValid(name)
        assertThat(RecordingUtils.flagOf(flags), `is`(RecordingUtils.NAME_INVALID))
        assertThat(RecordingUtils.charOf(flags), `is`(illegal))
    }
}