package com.nuclearpotato.soundrecorder.injection

import android.app.Application
import com.nuclearpotato.soundrecorder.model.RecordingStore
import com.nuclearpotato.soundrecorder.model.RecordingStoreImpl
import com.nuclearpotato.soundrecorder.playback.RecordingReader
import com.nuclearpotato.soundrecorder.playback.RecordingreaderImpl
import dagger.Module
import dagger.Provides
import java.io.File
import javax.inject.Singleton

@Module(includes = arrayOf(ApplicationModule::class))
class RecordingStoreModule{
    val folder = "recordings"

    @Provides
    @Singleton
    fun provideRecordingsDir(application: Application): File = File(application.getExternalFilesDir(null), folder)

    @Provides
    @Singleton
    fun provideRecordingStore(recordingsDir: File): RecordingStore = RecordingStoreImpl(recordingsDir)

    @Provides
    fun provideRecordingReader(store: RecordingStore): RecordingReader = RecordingreaderImpl(store)
}