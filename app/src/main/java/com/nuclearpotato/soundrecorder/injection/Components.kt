package com.nuclearpotato.soundrecorder.injection

import android.content.Context

object Components{

    fun soundRecorderComponent(context: Context) : SoundRecorderComponent {
        return getServiceOrThrow(context, SoundRecorderComponent.NAME)
    }

    @Suppress("UNCHECKED_CAST")
    private fun <T> getServiceOrThrow(context: Context, name: String): T {
        val myService = context.getSystemService(name) as T?
        if(myService != null) {
            return myService
        }

        val appService = context.applicationContext.getSystemService(name) as T?
        if(appService != null) {
            return appService
        }

        throw IllegalArgumentException("context does not provide a service named $name")
    }
}