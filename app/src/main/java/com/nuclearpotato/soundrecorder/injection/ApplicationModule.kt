package com.nuclearpotato.soundrecorder.injection

import android.app.Application
import dagger.Module
import dagger.Provides
import rx.Scheduler
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import javax.inject.Named
import javax.inject.Singleton

@Module
class ApplicationModule (private val application: Application){

    @Provides
    @Singleton
    fun provideApplication() = application

    @Provides
    @Singleton
    @Named(MAIN_SCHEDULER)
    fun provideMainScheduler(): Scheduler = AndroidSchedulers.mainThread()

    @Provides
    @Singleton
    @Named(IO_SCHEDULER)
    fun provideIoScheduler(): Scheduler = Schedulers.io()
}

const val MAIN_SCHEDULER = "main"
const val IO_SCHEDULER = "io"