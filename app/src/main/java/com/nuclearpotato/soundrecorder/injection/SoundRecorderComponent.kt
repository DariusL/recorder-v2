package com.nuclearpotato.soundrecorder.injection

import com.nuclearpotato.soundrecorder.playbackold.RecordingsListActivity
import com.nuclearpotato.soundrecorder.recording.RecordingService
import com.nuclearpotato.soundrecorder.save.SaveDialog
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(ApplicationModule::class, RecordingStoreModule::class))
interface SoundRecorderComponent {
    companion object {
        val NAME = SoundRecorderComponent::class.simpleName!!
    }

    fun inject(target: SaveDialog)
    fun inject(target: RecordingService)
    fun inject(target: RecordingsListActivity)
    fun inject(target: com.nuclearpotato.soundrecorder.playback.RecordingsListActivity)
}
