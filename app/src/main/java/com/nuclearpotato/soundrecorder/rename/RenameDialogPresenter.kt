package com.nuclearpotato.soundrecorder.rename

import android.content.Context
import com.nuclearpotato.soundrecorder.model.Recording
import com.nuclearpotato.soundrecorder.save.BaseSaveDialogPresenter
import com.nuclearpotato.soundrecorder.save.SaveDialogView

class RenameDialogPresenter(
        context: Context,
        recording: Recording,
        private val view: SaveDialogView,
        private val confirmCallback: (String) -> Unit ): BaseSaveDialogPresenter(context, recording, view) {

    fun onSaveClick() {
        confirmCallback(view.name)
    }

    fun onCancelClick() {

    }
}