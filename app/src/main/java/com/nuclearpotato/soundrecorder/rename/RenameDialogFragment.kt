package com.nuclearpotato.soundrecorder.rename

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AlertDialog
import com.nuclearpotato.soundrecorder.R
import com.nuclearpotato.soundrecorder.controls.BaseSaveDialog
import com.nuclearpotato.soundrecorder.model.Recording
import com.nuclearpotato.soundrecorder.save.SaveDialogView

class RenameDialogFragment : BaseSaveDialog(), SaveDialogView {

    private lateinit var presenter: RenameDialogPresenter

    private lateinit var callback: Callback

    override fun onCreateDialog(savedInstanceState: Bundle?): AlertDialog {
        callback = activity as Callback
        val dialog = super.onCreateDialog(savedInstanceState)
        presenter = RenameDialogPresenter(activity, recording()!!, this, {callback.onConfirm(it)})
        return dialog
    }

    override fun onPositiveClick() {
        presenter.onSaveClick()
    }

    override fun onNegativeClick() {
        presenter.onCancelClick()
    }

    interface Callback {
        fun onConfirm(name: String)
    }

    companion object {
        fun arguments(context: Context, recording: Recording): Bundle {
            return arguments(recording)
                .apply {
                    putString(ARG_POSITIVE_TEXT, context.getString(R.string.rename_dialog_save))
                    putString(ARG_NEGATIVE_TEXT, context.getString(R.string.rename_dialog_discard))
                    putString(ARG_TITLE, context.getString(R.string.rename_dialog_title))
                }
        }
    }
}