package com.nuclearpotato.soundrecorder

import android.content.Context
import android.os.Build
import android.support.annotation.ColorInt
import android.support.annotation.ColorRes
import java.util.*

object Util {

    fun join(list: Iterable<*>, delim: String): String {
        val sb = StringBuilder()

        var loopDelim = ""

        for (s in list) {

            sb.append(loopDelim)
            sb.append(s)

            loopDelim = delim
        }

        return sb.toString()
    }

    fun valid(name: String): Boolean {
        for (i in 0..C.INVALID_CHARS.length - 1)
            if (name.contains(C.INVALID_CHARS.substring(i, i + 1)))
                return false
        return true
    }

    @ColorInt
    fun getColor(context: Context, @ColorRes res: Int): Int {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return context.getColor(res)
        } else {
            //noinspection deprecation
            return context.resources.getColor(res)
        }
    }
}

fun <T> List<T>.toArrayList(): ArrayList<T> {
    return ArrayList(this)
}
