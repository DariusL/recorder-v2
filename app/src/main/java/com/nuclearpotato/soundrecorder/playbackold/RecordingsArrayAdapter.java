package com.nuclearpotato.soundrecorder.playbackold;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnErrorListener;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.nuclearpotato.soundrecorder.R;
import com.nuclearpotato.soundrecorder.model.Duration;
import com.nuclearpotato.soundrecorder.model.Recording;
import com.nuclearpotato.soundrecorder.model.RecordingStore;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RecordingsArrayAdapter extends ArrayAdapter<Recording> {

    private ArrayList<Recording> mRecordings = null;//holds all the recordings
    private ArrayList<Boolean> mSelected = null;//if the recording at i is selected, mSelected[i] = true
    private ArrayList<View> mViews = null;//holds all the created views for the list
    private int mSelectedCount = 0;//the amount of selected recs
    private Context mContext = null;
    private RecordingsListActivity mListActivity = null;
    private LayoutInflater mInflater = null;
    private ProgressDialog mProgress = null;//the Loading... thing
    private boolean mLoading = false;//true if recordings are being loaded
    private int mFilesProcessed = 0;//the number of recordings processed
    private int mPlaying = -1;//the id for the playing recording
    private MediaPlayer.OnPreparedListener mOnPrepared = null;
    private MediaPlayer.OnErrorListener mOnError = null;
    private MediaPlayer mPlayer = null;//the MediaPlayer used for preparation
    private final RecordingStore recordingStore;

    //creates a new adapter from objects, dismisses the dialog when finished
    public RecordingsArrayAdapter(Context context, RecordingStore recordingStore, ProgressDialog dialog){
        super(context, 0);
        this.recordingStore = recordingStore;
        mProgress = dialog;
        mContext = context;
        mListActivity = (RecordingsListActivity) context;
        load(recordingStore.listRecordings());

        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        if(mViews.get(pos) == null){
            View view = mInflater.inflate(R.layout.recordings_list_item, parent, false);
            TextView titleText = (TextView)view.findViewById(R.id.title_text);
            TextView subtitleText = (TextView)view.findViewById(R.id.subtitle_text);
            titleText.setText(mRecordings.get(pos).getName());
            Duration duration = mRecordings.get(pos).getDuration();
            subtitleText.setText(formatDuration(duration));
            mViews.set(pos, view);
        }
        Resources r = mContext.getResources();
        if(mSelected.get(pos) == true)
            mViews.get(pos).setBackgroundColor(r.getColor(R.color.my_light_blue));
        else{
            if(pos == mPlaying)
                mViews.get(pos).setBackgroundColor(r.getColor(R.color.my_light_gray));
            else
                mViews.get(pos).setBackgroundColor(r.getColor(R.color.my_app_back));
        }
        return mViews.get(pos);
    }

    @NonNull
    private String formatDuration(@Nullable Duration duration) {
        return duration != null ? String.valueOf(duration.getMillis()) : "--";
    }

    @Override
    public int getCount(){
        return mRecordings.size();
    }

    public void deleteSelected(){

        for(int i = 0; i < mRecordings.size(); i++){
            if(mSelected.get(i) == true)
            {
                recordingStore.delete(mRecordings.get(i));
                if(i < mPlaying)
                    mPlaying--;
            }
        }
        for(int i = mRecordings.size() - 1; i >= 0; i--){
            if(mSelected.get(i) == true){
                mRecordings.remove(i);
                mSelected.remove(i);
                mViews.remove(i);
            }
        }
        notifyDataSetChanged();
    }

    public void shareSelected(){
        if(mSelectedCount < 1)
            return;
        Intent shareIntent = new Intent();
        shareIntent.setType("audio/mpeg");
        if(mSelectedCount == 1){
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(recordingStore.file(mRecordings.get(getSelectedId()))));
        }
        else{
            ArrayList<Uri> files = new ArrayList<Uri>(mSelectedCount);
            for(int i = 0; i < mRecordings.size(); i++)
                if(mSelected.get(i) == true){
                    files.add(Uri.fromFile(recordingStore.file(mRecordings.get(getSelectedId()))));
                }
            shareIntent.setAction(Intent.ACTION_SEND_MULTIPLE);
            shareIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, files);
        }
        mListActivity.startActivity(Intent.createChooser(shareIntent, "Share via..."));
    }

    public void selectItem(int pos){
        boolean a = mSelected.get(pos);
        mSelected.set(pos, !a);
        if(a)
            mSelectedCount--;
        else
            mSelectedCount++;
        notifyDataSetChanged();
    }

    public void selectAll(){
        Collections.fill(mSelected, true);
        mSelectedCount = mSelected.size();
        notifyDataSetChanged();
    }

    public void deselctAll(){
        Collections.fill(mSelected, false);
        mSelectedCount = 0;
        notifyDataSetChanged();
    }

    public int getSelectedCount(){
        return mSelectedCount;
    }

    public Recording getRecording(int pos){
        return mRecordings.get(pos);
    }

    public void setPlaying(int id){
        mPlaying = id;
        notifyDataSetChanged();
    }

    public int getPlaying(){
        return mPlaying;
    }

    public boolean isPlayingSelected(){
        if(mPlaying < 0)
            return false;
        return mSelected.get(mPlaying);
    }

    public String getSelectedName(){
        return mRecordings.get(getSelectedId()).getName();
    }

    public int getSelectedId(){
        int s = -1;
        for(int i = 0; i < mRecordings.size(); i++)
            if(mSelected.get(i) == true){
                s = i;
                break;
            }
        return s;
    }

    public void renameItem(String to, int pos){
        recordingStore.rename(mRecordings.get(pos), to);
        mViews.set(pos, null);
        notifyDataSetChanged();
    }

    private void load(List<Recording> files){
        if(files.size() == 0)
            return;
        mPlayer = new MediaPlayer();
        mFilesProcessed = 0;
        mOnError = new OnErrorListener(){

            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                Recording recording = mRecordings.get(mFilesProcessed);
                mRecordings.set(mFilesProcessed, new Recording(recording.getName(), 0));
                mFilesProcessed++;
                loadMoar();
                return true;
            }

        };
        mPlayer.setOnErrorListener(mOnError);
        mOnPrepared = new MediaPlayer.OnPreparedListener() {

            @Override
            public void onPrepared(MediaPlayer mp) {
                try{
                    Recording recording = mRecordings.get(mFilesProcessed);
                    mRecordings.set(mFilesProcessed, new Recording(recording.getName(), mPlayer.getDuration()));
                } catch (IllegalStateException e){
                    e.printStackTrace();
                    Recording recording = mRecordings.get(mFilesProcessed);
                    mRecordings.set(mFilesProcessed, new Recording(recording.getName(), 0));
                }
                mFilesProcessed++;
                loadMoar();
            }
        };
        mPlayer.setOnPreparedListener(mOnPrepared);
        mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);


        mLoading = true;

        mRecordings = new ArrayList<>(files);
        mViews = new ArrayList<View>(files.size());
        mSelected = new ArrayList<Boolean>(files.size());
        for(int i = 0; i < files.size(); i++){
            mSelected.add(false);
            mViews.add(null);
        }

        loadMoar();

    }
    public void loadMoar(){
        mPlayer.reset();
        boolean shitFailed = false;
        if(mFilesProcessed == mRecordings.size()){
            mLoading = false;
            notifyDataSetChanged();
            mProgress.dismiss();
            mOnError = null;
            mOnPrepared = null;
            mPlayer = null;
        } else{
            try {
                mPlayer.setDataSource(recordingStore.file(mRecordings.get(mFilesProcessed)).getAbsolutePath());
            } catch (Exception e) {
                e.printStackTrace();
                shitFailed = true;
                Recording recording = mRecordings.get(mFilesProcessed);
                mRecordings.set(mFilesProcessed, new Recording(recording.getName(), 0));
                mFilesProcessed++;
            }
            if(!shitFailed)
                mPlayer.prepareAsync();
            else
                loadMoar();
        }
    }
}