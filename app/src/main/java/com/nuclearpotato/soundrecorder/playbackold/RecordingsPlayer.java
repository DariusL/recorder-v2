package com.nuclearpotato.soundrecorder.playbackold;

import android.media.AudioManager;
import android.media.MediaPlayer;

import com.nuclearpotato.soundrecorder.model.Recording;
import com.nuclearpotato.soundrecorder.model.RecordingStore;

public class RecordingsPlayer {
    private MediaPlayer mPlayer = null;
    private boolean mPaused = false;
    private long mTimeStarted = 0;
    private long mTimeElapsed = 0;
    private boolean mPlaying = false;
    private Recording mRecording = null;
    private final RecordingStore recordingStore;

    public RecordingsPlayer(RecordingStore recordingStore) {
        this.recordingStore = recordingStore;
        mPlayer = new MediaPlayer();
        mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

        mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mPaused = false;
                mPlaying = false;
            }
        });

    }

    public void play(Recording rec){
        if(mPlaying)
            mPlayer.stop();
        mRecording = rec;
        mPaused = false;
        mPlayer.reset();
        mPlaying = true;
        try {
            mPlayer.setDataSource(recordingStore.file(rec).getAbsolutePath());
            mPlayer.prepare();
        } catch (Exception e) {
            e.printStackTrace();
        }

        mTimeStarted = System.currentTimeMillis();
        mTimeElapsed = 0;
        mPlayer.start();
    }

    public int getProgress(){
        if(!mPlaying || mRecording.getDuration().getMillis() <= 0L)
            return 0;
        if(mPaused)
            return (int)((mTimeElapsed) / (float)mRecording.getDuration().getMillis() * 100);
        else
            return (int)((mTimeElapsed + System.currentTimeMillis() - mTimeStarted) / (float)mRecording.getDuration().getMillis() * 100);
    }

    public String getProgressString(){
        if(!mPlaying)
            return "00:00:00";
        int seconds;
        if(mPaused)
            seconds = (int) (mTimeElapsed / 1000);
        else
            seconds = (int)((mTimeElapsed + System.currentTimeMillis() - mTimeStarted) / 1000);
        int minutes = seconds / 60;
        int hours = minutes / 60;
        minutes %= 60;
        seconds %= 60;
        return String.format("%02d:%02d:%02d", hours, minutes, seconds);
    }

    public String getName(){
        if(mRecording != null)
            return mRecording.getName();
        else
            return "";
    }

    public String getDurationString(){
        if(mRecording == null)
            return "00:00:00";
        int seconds = (int)(mRecording.getDuration().getMillis() / 1000);
        int minutes = seconds / 60;
        int hours = minutes / 60;
        minutes %= 60;
        seconds %= 60;
        return String.format("%02d:%02d:%02d", hours, minutes, seconds);
    }

    public void buttonPressed(){
        if(mRecording == null)
            return;
        if(!mPlaying && mRecording != null){
            play(mRecording);
            return;
        }
        if(!mPaused){
            mPaused = true;
            mTimeElapsed = mTimeElapsed + System.currentTimeMillis() - mTimeStarted;
            mPlayer.pause();
        }
        else{
            mPaused = false;
            mTimeStarted = System.currentTimeMillis();
            mPlayer.start();
        }
    }

    public void seekTo(int n){
        if(mPlaying){
            mTimeStarted = System.currentTimeMillis();
            int seek = (int) (mRecording.getDuration().getMillis() * n / 100);
            mTimeElapsed = seek;
            mPlayer.seekTo(seek);
        }
    }

    public void stop(){
        if(mPlaying && !mPaused)
            mPlayer.stop();
        mRecording = null;
        mPaused = false;
        mPlaying = false;
    }

    public void release(){
        stop();
        mPlayer.release();
    }

    public boolean isPlaying(){
        return mPlaying;
    }

    public boolean isPaused(){
        return mPaused || !mPlaying;
    }
}