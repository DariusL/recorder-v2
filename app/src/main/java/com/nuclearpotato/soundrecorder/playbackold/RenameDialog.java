package com.nuclearpotato.soundrecorder.playbackold;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.nuclearpotato.soundrecorder.R;
import com.nuclearpotato.soundrecorder.Util;

public class RenameDialog extends Dialog {

    private Button mAcceptButton = null;
    private Button mCancelButton = null;
    private EditText mEdit = null;
    private RecordingsListActivity mContext = null;
    private int mId = -1;

    public RenameDialog(Context context) {
        super(context);
        mContext = (RecordingsListActivity) context;
        setContentView(R.layout.dialog_layout);
        setTitle("Rename Recording");
        mEdit = (EditText)findViewById(0);
        mEdit.setFocusable(true);
        mAcceptButton = (Button)findViewById(0);
        mAcceptButton.setText("Accept");
        mAcceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String name = mEdit.getText().toString();
                if(name.length() == 0){
                    Toast.makeText(mContext, "Please input a name", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(!Util.INSTANCE.valid(name)){
                    Toast.makeText(mContext, "\\ / : * ? \" < > | are not allowed", Toast.LENGTH_SHORT).show();
                    return;
                }

                rename();
                dismiss();
            }
        });
        
        mCancelButton = (Button)findViewById(0);
        mCancelButton.setText("Cancel");
        mCancelButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE|WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        
        setOnShowListener(new DialogInterface.OnShowListener() {

            @Override
            public void onShow(DialogInterface dialog) {
                mEdit.selectAll();
                mEdit.requestFocus();
            }
        });
        
        mEdit.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
            }
        });
    }
    private void rename(){
        mContext.mArrayAdapter.renameItem(mEdit.getText().toString(), mId);
    }
    public void setFileName(String fileName, int i){
        mEdit.setText(fileName);
        mId = i;
    }
}