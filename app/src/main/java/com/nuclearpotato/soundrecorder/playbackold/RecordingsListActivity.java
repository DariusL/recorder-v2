package com.nuclearpotato.soundrecorder.playbackold;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.nuclearpotato.soundrecorder.C;
import com.nuclearpotato.soundrecorder.R;
import com.nuclearpotato.soundrecorder.injection.Components;
import com.nuclearpotato.soundrecorder.model.RecordingStore;
import com.nuclearpotato.soundrecorder.recording.SoundRecorderActivity;

import javax.inject.Inject;

public class RecordingsListActivity extends ActionBarActivity {
    private RenameDialog mDialog = null;
    private ActionMode mActionMode = null;
    private ListView mListView = null;
    public RecordingsArrayAdapter mArrayAdapter = null;
    private UberActionModeCallback mActionCallback = null;
    private Button mPlayButton = null;
    private TextView mPlayingText = null;
    private SeekBar mSeekBar = null;
    private Handler mHandler = new Handler();
    private RecordingsPlayer mPlayer = null;
    private TextView mTv[] = null;
    private ImageView mButtonImage = null;
    private ProgressDialog mProgress = null;

    @Inject
    RecordingStore store;

    private final class UberActionModeCallback implements ActionMode.Callback {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            getMenuInflater().inflate(R.menu.recordings_cab, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch(item.getItemId()){
            case R.id.cab_delete:
                if(mArrayAdapter.isPlayingSelected())
                {
                    mPlayer.stop();
                    mArrayAdapter.setPlaying(-1);
                }
                mArrayAdapter.deleteSelected();
                mActionMode.finish();
                break;
            case R.id.cab_share:
                mArrayAdapter.shareSelected();
                break;
            case R.id.cab_edit:
                if(mDialog != null)
                    mDialog.setFileName(mArrayAdapter.getSelectedName(), mArrayAdapter.getSelectedId());
                if(mArrayAdapter.isPlayingSelected()){
                    mPlayer.stop();
                    mArrayAdapter.setPlaying(-1);
                }
                showDialog(C.RENAME_DIALOG);
                mActionMode.finish();
                break;
            case R.id.cab_sel:
                mArrayAdapter.selectAll();
                mActionMode.setTitle(mArrayAdapter.getSelectedCount() + " Selected");
                Menu menu = mActionMode.getMenu();
                menu.clear();
                if(mArrayAdapter.getSelectedCount() != 1)
                    getMenuInflater().inflate(R.menu.recordings_cab_no_rename, menu);
                else
                    getMenuInflater().inflate(R.menu.recordings_cab, menu);
                menu = null;
                break;
            }
            return true;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            RecordingsListActivity.this.mArrayAdapter.deselctAll();
            mActionMode = null;
        }
        
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Components.INSTANCE.soundRecorderComponent(this).inject(this);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Recordings");

        setContentView(R.layout.recordings_list_old);

        mProgress = ProgressDialog.show(this, "", "Loading. Please wait..." , true, false);
        mProgress.setCancelable(true);
        mProgress.setCanceledOnTouchOutside(false);
        mProgress.setOnCancelListener(new OnCancelListener() {

            @Override
            public void onCancel(DialogInterface dialog) {
                Intent intent = new Intent(getApplicationContext(), SoundRecorderActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

            }
        });

        mActionCallback = new UberActionModeCallback();

        mButtonImage = (ImageView)findViewById(R.id.imageView1);

        mListView = (ListView) findViewById(android.R.id.list);
        mListView.setDivider(null);
        mListView.setDividerHeight(0);
        mListView.setOnItemClickListener(onListItemClick);
        loadFiles();
        mListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> av, View v, int pos, long id) {
                if (mActionMode != null) {
                    return false;
                }
                mActionMode = RecordingsListActivity.this.startSupportActionMode(mActionCallback);
                mActionMode.setTitle("1 Selected");
                mArrayAdapter.selectItem((int)id);
                return true;
            }
        });

        mPlayingText = (TextView)findViewById(R.id.playback_text);
        mPlayButton = (Button)findViewById(R.id.play_pause);
        mPlayButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                mPlayer.buttonPressed();
                if(mPlayer.isPaused())
                    mButtonImage.setImageResource(R.drawable.play);
                else
                    mButtonImage.setImageResource(R.drawable.pause);
            }
        });
        mSeekBar = (SeekBar)findViewById(R.id.seek_bar);
        mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                mPlayer.seekTo(seekBar.getProgress());
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                    boolean fromUser) {
                if(fromUser)
                    mPlayer.seekTo(progress);
            }
        });
        mTv = new TextView[2];

        mTv[0] = (TextView)findViewById(R.id.play_prog);
        mTv[0].setText("00:00:00");
        mTv[1] = (TextView)findViewById(R.id.play_dur);
        mTv[1].setText("00:00:00");

        mHandler.postDelayed(mUpdateTask, 100);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPlayer = new RecordingsPlayer(store);
    }

    private AdapterView.OnItemClickListener onListItemClick = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            if(mActionMode != null){
                mArrayAdapter.selectItem((int) id);
                mActionMode.setTitle(mArrayAdapter.getSelectedCount() + " Selected");
                Menu menu = mActionMode.getMenu();
                if(mArrayAdapter.getSelectedCount() == 1){
                    menu.clear();
                    getMenuInflater().inflate(R.menu.recordings_cab, menu);
                }
                else{
                    menu.clear();
                    getMenuInflater().inflate(R.menu.recordings_cab_no_rename, menu);
                }
                if(mArrayAdapter.getSelectedCount() == 0)
                    mActionMode.finish();
                return;
            }

            mPlayer.play(mArrayAdapter.getRecording((int) id));
            mArrayAdapter.setPlaying((int) id);
            mPlayingText.setText(mArrayAdapter.getRecording((int) id).getName());
        }
    };

    @Override
    public void onStop(){
        if(mPlayer != null){
            mPlayer.release();
            mPlayer = null;
        }
        if(mHandler != null)
            mHandler.removeCallbacks(mUpdateTask);
        if(mProgress != null)
            mProgress.dismiss();
        super.onStop();
    }

    @Override
    protected Dialog onCreateDialog(int id){
        Dialog dialog = null;
        switch(id){
        case C.RENAME_DIALOG:
            dialog = new RenameDialog(this);
            ((RenameDialog) dialog).setFileName(mArrayAdapter.getSelectedName(), mArrayAdapter.getSelectedId());
            mDialog = (RenameDialog)dialog;
            break;
        }
        return dialog;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch(item.getItemId()){
        case android.R.id.home:
            Intent intent = new Intent(this, SoundRecorderActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }

    public void loadFiles(){
        mArrayAdapter = new RecordingsArrayAdapter(this, store, mProgress);
        mListView.setAdapter(mArrayAdapter);
    }

    private Runnable mUpdateTask = new Runnable(){

        @Override
        public void run() {

            mSeekBar.setProgress(mPlayer.getProgress());

            mTv[0].setText(mPlayer.getProgressString());
            mTv[1].setText(mPlayer.getDurationString());

            mPlayingText.setText(mPlayer.getName());

            if(mPlayer.isPaused())
                mButtonImage.setImageResource(R.drawable.play);
            else
                mButtonImage.setImageResource(R.drawable.pause);

            mHandler.postDelayed(mUpdateTask, 500);
        }
    };
}