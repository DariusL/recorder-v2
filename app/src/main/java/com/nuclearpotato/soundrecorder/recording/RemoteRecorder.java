package com.nuclearpotato.soundrecorder.recording;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;

import com.nuclearpotato.soundrecorder.model.Recording;

import org.jetbrains.annotations.NotNull;
import org.joda.time.DateTime;

class RemoteRecorder implements RecorderConnection {

    // client whats
    public static final int WHAT_START = 1002;
    public static final int WHAT_STOP = 1003;
    public static final int WHAT_BIND = 1004;

    private Messenger incomingMessenger = new Messenger(new IncomingHandler());
    private Messenger outgoingMessenger;

    private final Context context;
    private Callbacks callback;

    private DateTime startedTime;

    RemoteRecorder(Context context) {
        this.context = context;
    }

    @Override
    public void connect(@NotNull Callbacks callback) {
        this.callback = callback;
        Intent intent = new Intent(context, RecordingService.class);
        context.startService(intent);
        context.bindService(intent, connection, Context.BIND_IMPORTANT | Context.BIND_ABOVE_CLIENT);
    }

    @Override
    public void disconnect() {
        callback = null;
        context.unbindService(connection);
    }

    @Override
    public void start() {
        Message message = Message.obtain(null, RemoteRecorder.WHAT_START);
        send(message);
    }

    @Override
    public void stop() {
        Message message = Message.obtain(null, RemoteRecorder.WHAT_STOP);
        send(message);
    }

    private class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what){
                case RecordingService.WHAT_PREPARING:
                    // pointless
                    break;
                case RecordingService.WHAT_DONE:
                    Bundle data = msg.getData();
                    callback.onRecordingComplete(new Recording(RecordingService.getRecording(data).getName(), System.currentTimeMillis() - startedTime.getMillis()));
                    break;
                case RecordingService.WHAT_RECORDING:
                    startedTime = RecordingService.getStartedTime(msg.getData());
                    callback.onRecording(startedTime.getMillis());
                    break;
                case RecordingService.WHAT_READY:
                    callback.onPrepared();
                    break;
                case RecordingService.WHAT_ERROR:
                    throw new UnsupportedOperationException("");
                default:
                    throw new UnsupportedOperationException("Unknown service what " + msg.what);
            }
        }
    }

    private ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            RemoteRecorder.this.outgoingMessenger = new Messenger(service);

            Message message = Message.obtain(null, RemoteRecorder.WHAT_BIND);
            message.replyTo = RemoteRecorder.this.incomingMessenger;
            send(message);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            outgoingMessenger = null;
        }
    };

    private void send(Message message){
        try {
            outgoingMessenger.send(message);
        } catch (RemoteException e){
            handle(e);
        }
    }

    private void handle(RemoteException e){
        throw new RuntimeException(e);
    }
}
