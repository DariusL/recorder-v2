package com.nuclearpotato.soundrecorder.recording

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.nuclearpotato.soundrecorder.AboutScreenActivity
import com.nuclearpotato.soundrecorder.R
import com.nuclearpotato.soundrecorder.model.Duration
import com.nuclearpotato.soundrecorder.model.Recording
import com.nuclearpotato.soundrecorder.playback.RecordingsListActivity
import com.nuclearpotato.soundrecorder.save.SaveDialog
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.util.*

class SoundRecorderActivity : AppCompatActivity(), RecordingView {

    @BindView(R.id.textview_timer)
    internal lateinit  var timeText: TextView
    @BindView(R.id.button_record)
    internal lateinit  var recordButton: Button

    private val recorderConnection = RemoteRecorder(this)
    private lateinit var presenter: RecordingPresenter

    private val clock = {
        System.currentTimeMillis()
    }

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        ButterKnife.bind(this)
        presenter = RecordingPresenter(recorderConnection, this, clock, Schedulers.computation(), AndroidSchedulers.mainThread())
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_about -> {
                startActivity(Intent(this, AboutScreenActivity::class.java))
                return true
            }
            R.id.menu_recordings -> {
                startActivity(Intent(this, RecordingsListActivity::class.java))
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onStart() {
        super.onStart()
        presenter.onStart()
    }

    override fun onStop() {
        super.onStop()
        presenter.onStop()
    }

    @OnClick(R.id.button_record)
    internal fun onRecordClicked() {
        presenter.onButtonClick()
    }

    override fun enableButton(enable: Boolean) {
        recordButton.isEnabled = enable
    }

    override fun setRecording(recording: Boolean?) {
        val isRecording = java.lang.Boolean.TRUE == recording
        timeText.visibility = if (isRecording) View.VISIBLE else View.INVISIBLE
        recordButton.setText(if (isRecording) R.string.button_rec_stop else R.string.button_rec_start)
    }

    override fun saveRecording(recording: Recording) {
        SaveDialog.newInstance(this, recording).show(supportFragmentManager, null)
    }

    override fun setRecordingTime(time: Long) {
        val d = Duration(time)
        timeText.text = String.format(Locale.US, "%02d : %02d : %02d", d.getHours(), d.getMinutes(), d.getSeconds())
    }
}