package com.nuclearpotato.soundrecorder.recording

import com.nuclearpotato.soundrecorder.model.Recording
import org.joda.time.DateTime

class MockRecorder : RecorderConnection {
    private var callbacks: RecorderConnection.Callbacks? = null
    private var startedTime: DateTime? = null

    override fun connect(callback: RecorderConnection.Callbacks) {
        this.callbacks = callback
        if(isRecording()) {
            callbacks!!.onRecording(startMillis())
        } else {
            callbacks!!.onPrepared()
        }
    }

    override fun disconnect() {
        this.callbacks = null
    }

    override fun start() {
        if(isRecording()) {
            throw IllegalStateException("start called while recording")
        }
        startedTime = DateTime.now()
        callbacks!!.onRecording(startMillis())
    }

    override fun stop() {
        if(!isRecording()) {
            throw IllegalStateException("stop called while not recording")
        }
        callbacks!!.onRecordingComplete(Recording("foo", duration()))
        startedTime = null
    }

    private fun duration() = DateTime.now().millis - startMillis()

    private fun startMillis() = startedTime!!.millis

    private fun isRecording() = startedTime != null
}

