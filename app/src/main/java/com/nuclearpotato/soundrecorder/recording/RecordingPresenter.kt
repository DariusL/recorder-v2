package com.nuclearpotato.soundrecorder.recording

import com.nuclearpotato.soundrecorder.model.Recording
import rx.Observable
import rx.Scheduler
import rx.Subscription
import java.util.concurrent.TimeUnit

class RecordingPresenter (
        private val connection: RecorderConnection,
        private val view: RecordingView,
        private val currentTime: () -> Long,
        private val retryScheduler: Scheduler,
        private val mainScheduler: Scheduler) : RecorderConnection.Callbacks {

    private var recording: Boolean? = null
    private var timerSubscription: Subscription? = null

    override fun onPrepared() {
        setRecording(false)
        view.setRecording(false)
    }

    override fun onRecording(startTime: Long) {
        setRecording(true)
        view.setRecording(true)
        startTimer(startTime)
    }

    override fun onRecordingComplete(recording: Recording) {
        setRecording(false)
        view.setRecording(false)
        view.saveRecording(recording)
        stopTimer()
    }

    fun onStart(){
        connection.connect(this)
        setRecording(null)
        view.setRecording(null)
    }

    fun onStop() {
        connection.disconnect()
        stopTimer()
    }

    fun onButtonClick() {
        when (recording) {
            true -> connection.stop()
            false -> connection.start()
            null -> throw IllegalStateException("Recording button was clicked in an invalid state")
        }
        setRecording(null)
    }

    private fun setRecording(recording: Boolean?) {
        this.recording = recording
        view.enableButton(recording != null)
    }

    private fun startTimer(recordingStartTime: Long) {
        timerSubscription = Observable
                .defer { Observable.just(currentTime() - recordingStartTime) }
                .repeatWhen(
                        { completions -> completions.delay(1000, TimeUnit.MILLISECONDS, retryScheduler) },
                        retryScheduler
                )
                .observeOn(mainScheduler)
                .subscribe { view.setRecordingTime(it) }
    }

    private fun stopTimer() {
        timerSubscription?.unsubscribe()
        timerSubscription = null
    }
}