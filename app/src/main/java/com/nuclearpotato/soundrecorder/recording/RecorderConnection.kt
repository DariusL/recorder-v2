package com.nuclearpotato.soundrecorder.recording

import com.nuclearpotato.soundrecorder.model.Recording

interface RecorderConnection {
    fun connect(callback: Callbacks)
    fun disconnect()
    fun start()
    fun stop()

    interface Callbacks {
        fun onPrepared()
        fun onRecording(startTime: Long)
        fun onRecordingComplete(recording: Recording)
    }
}
