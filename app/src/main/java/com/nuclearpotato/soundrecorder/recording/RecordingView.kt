package com.nuclearpotato.soundrecorder.recording

import com.nuclearpotato.soundrecorder.model.Recording

interface RecordingView {
    fun enableButton(enable: Boolean)
    fun setRecording(recording: Boolean?)
    fun saveRecording(recording: Recording)
    /**
     * @param time recording length in milliseconds
     */
    fun setRecordingTime(time: Long)
}