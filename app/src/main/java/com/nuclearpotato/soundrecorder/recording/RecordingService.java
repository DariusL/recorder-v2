package com.nuclearpotato.soundrecorder.recording;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.nuclearpotato.soundrecorder.R;
import com.nuclearpotato.soundrecorder.injection.Components;
import com.nuclearpotato.soundrecorder.model.Recording;
import com.nuclearpotato.soundrecorder.model.RecordingStore;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.IOException;
import java.util.Locale;

import javax.inject.Inject;

public class RecordingService extends Service{
    private static final String LOG_TAG = RecordingService.class.getName();
    private static final int NOTIF_ID = 1;
    private static final int BIT_RATE = 131072;
    public static final String FOLDER = "recordings";

    public static final DateTimeFormatter UNNAMED_FORMAT = DateTimeFormat.forPattern("'Unnamed_'yyyy_MM_dd_HH_mm_ss").withLocale(Locale.US);

    // service whats
    public static final int WHAT_PREPARING = 2001;
    public static final int WHAT_DONE = 2002;
    public static final int WHAT_READY = 2003;
    public static final int WHAT_RECORDING = 2004;
    public static final int WHAT_ERROR = 2005;

    private static final String EXTRA_STARTED_TIME = "RecordingService.extra.started_time";
    private static final String EXTRA_NAME = "RecordingService.extra.filename";

    private static final String ACTION_STOP = "RecordingService.action.stop";

    private MediaRecorder recorder;
    private State state;
    private DateTime started;

    @Inject
    RecordingStore recordingStore;
    private final Handler handler = new Handler();
    private final Messenger messenger = new Messenger(new IncomingHandler());
    private Messenger client;
    private @Nullable Recording currentRecording;

    private enum State{
        PREPARING, READY, RECORDING, ERROR
    }

    private class IncomingHandler extends Handler{

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what){
                case RemoteRecorder.WHAT_BIND:
                    client = msg.replyTo;
                    sendState();
                    break;
                case RemoteRecorder.WHAT_START:
                    verifyBound();
                    start();
                    break;
                case RemoteRecorder.WHAT_STOP:
                    verifyBound();
                    stop();
                    break;
                default:
                    throw new UnsupportedOperationException("Unknown incoming what " + msg.what);
            }
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        goBackground();
        return messenger.getBinder();
    }

    @Override
    public void onRebind(Intent intent) {
        goBackground();
        super.onRebind(intent);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        client = null;
        if(state == State.RECORDING) {
            goForeground();
            return true;
        }else{
            stopSelf();
            return false;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Components.INSTANCE.soundRecorderComponent(this).inject(this);

        if (recordingStore.setup()) {
            prepare();
        } else {
            onFail("Failed to setup recordings dir");
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if(intent != null && ACTION_STOP.equals(intent.getAction())){
            // notification stop button
            // service should be recording and foreground
            stop();
            stopSelf();
        }
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(state == State.RECORDING){
            Log.e(LOG_TAG, "Service was destroyed while recording!");
            stop();
        }
        release();

        if(currentRecording != null) {
            recordingStore.delete(currentRecording);
        }
    }

    private void goBackground(){
        stopForeground(true);
    }

    private void goForeground(){
        Intent intent = new Intent(this, SoundRecorderActivity.class);
        PendingIntent pi = PendingIntent.getActivity(this, 0, intent, 0);

        Intent stopIntent = new Intent(ACTION_STOP, null, this, RecordingService.class);
        PendingIntent spi = PendingIntent.getService(this, 0, stopIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Notification notification = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.notif_icon)
                .setContentTitle("TEMP")
                .setContentIntent(pi)
                .addAction(R.drawable.ic_stop_grey600_36dp, getString(R.string.notification_button_stop), spi)
                .build();

        startForeground(NOTIF_ID, notification);
    }


    private void prepare(){
        setState(State.PREPARING);
        new Thread(new Runnable() {
            @Override
            public void run() {
                //TODO: use reset maybe
                release();

                currentRecording = recordingStore.create();
                recorder = new MediaRecorder();

                recorder.setOnErrorListener(new MediaRecorder.OnErrorListener() {
                    @Override
                    public void onError(MediaRecorder mr, int what, int extra) {
                        switch (what){
                            case MediaRecorder.MEDIA_ERROR_SERVER_DIED:
                                // thrown when stop is called immediately after start, handled in stop()
                                Toast.makeText(RecordingService.this, R.string.toast_server_died, Toast.LENGTH_SHORT).show();
                                break;
                            case MediaRecorder.MEDIA_RECORDER_ERROR_UNKNOWN:
                                onFail(String.format("MediaRecorder error, what: %d, extra: %d", what, extra));
                                break;
                        }
                    }
                });
                recorder.setOnInfoListener(new MediaRecorder.OnInfoListener() {
                    @Override
                    public void onInfo(MediaRecorder mr, int what, int extra) {
                        Log.d(LOG_TAG, String.format("MediaRecorder unhandled info what: %d, extra: %d", what, extra));
                        // TODO cap size according to storage space
                    }
                });

                recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
                recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
                recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
                recorder.setAudioEncodingBitRate(BIT_RATE);
                recorder.setAudioChannels(1);
                recorder.setOutputFile(recordingStore.file(currentRecording).getAbsolutePath());
                try {
                    recorder.prepare();
                } catch (IOException e) {
                    onFail(e.getMessage());
                    e.printStackTrace();
                }

                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        onPrepared();
                    }
                });
            }
        }).start();
    }

    private void release(){
        if(recorder != null) {
            recorder.reset();
            recorder.release();
            recorder = null;
        }
    }

    private void verifyBound(){
        if(client == null){
            throw new IllegalStateException("Service is not bound!");
        }
    }

    public void setState(State state) {
        this.state = state;
        if(client != null){
            sendState();
        }
    }

    private void sendState(){
        Message message = Message.obtain();
        switch (state){
            case PREPARING:
                message.what = WHAT_PREPARING;
                break;
            case READY:
                message.what = WHAT_READY;
                break;
            case RECORDING:
                message.what = WHAT_RECORDING;
                Bundle bundle = new Bundle();
                bundle.putLong(EXTRA_STARTED_TIME, started.getMillis());
                message.setData(bundle);
                break;
            case ERROR:
                message.what = WHAT_ERROR;
                break;
        }
        try {
            client.send(message);
        } catch (RemoteException e) {
            handleException(e);
        }
    }

    private void onPrepared(){
        setState(State.READY);
    }

    private void onFail(String message){
        throw new RuntimeException(message);
        //setState(State.ERROR);
        //stopSelf();
    }

    private void start(){
        if(state != State.READY){
            throw new IllegalStateException();
        }

        started = DateTime.now();
        recorder.start();
        setState(State.RECORDING);
    }

    private void stop(){
        try {
            recorder.stop();

            if(client != null) {
                doneWithUi();
            }else{
                doneWithoutUi();
            }
        } catch (IllegalStateException up){
            throw up;
        } catch (RemoteException e) {
            handleException(e);
        } catch (RuntimeException e){
            // Note that a RuntimeException is intentionally thrown to the application,
            // if no valid audio/video data has been received when stop() is called.
            e.printStackTrace();
            recordingStore.delete(currentRecording);
        }

        prepare();
    }

    private void doneWithUi() throws  RemoteException{
        Message message = Message.obtain();
        message.what = WHAT_DONE;
        Bundle bundle = new Bundle();
        bundle.putString(EXTRA_NAME, currentRecording.getName());
        message.setData(bundle);
        client.send(message);
    }

    private void doneWithoutUi(){
        currentRecording = recordingStore.rename(currentRecording, UNNAMED_FORMAT.print(DateTime.now()));
        // TODO handle failure
        Toast.makeText(this, String.format(getString(R.string.toast_recording_saved), currentRecording.getName()), Toast.LENGTH_SHORT).show();
    }

    private void handleException(RemoteException e){
        e.printStackTrace();
        Log.e(LOG_TAG, "Messaging the client failed, stopping");
        stopSelf();
    }

    public static DateTime getStartedTime(Bundle data){
        return new DateTime(data.getLong(EXTRA_STARTED_TIME));
    }

    public static Recording getRecording(Bundle data){
        return new Recording(data.getString(EXTRA_NAME), null);
    }
}
