package com.nuclearpotato.soundrecorder

import android.app.Application
import com.nuclearpotato.soundrecorder.injection.ApplicationModule
import com.nuclearpotato.soundrecorder.injection.DaggerSoundRecorderComponent
import com.nuclearpotato.soundrecorder.injection.SoundRecorderComponent

class SoundRecorderApplication : Application() {

    private lateinit var applicationComponent: SoundRecorderComponent

    override fun onCreate() {
        super.onCreate()

        applicationComponent = DaggerSoundRecorderComponent.builder()
                .applicationModule(ApplicationModule(this))
                .build()
    }

    override fun getSystemService(name: String?): Any? {
        return when(name) {
            SoundRecorderComponent.NAME -> applicationComponent
            else -> super.getSystemService(name)
        }
    }
}