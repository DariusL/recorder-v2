package com.nuclearpotato.soundrecorder.model

import nz.bradcampbell.paperparcel.PaperParcel

@PaperParcel
data class Recording(val name: String, val duration: Duration?){
    constructor(name: String, duration: Long) : this(name, Duration(duration))
}