package com.nuclearpotato.soundrecorder.model

import nz.bradcampbell.paperparcel.PaperParcel

@PaperParcel
data class Duration(val millis: Long) {
    fun getHours() = millis / (1000 * 60 * 60)
    fun getMinutes() = millis / (1000 * 60) % 60
    fun getSeconds() = millis / 1000 % 60
}
