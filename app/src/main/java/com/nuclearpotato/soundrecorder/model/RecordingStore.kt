package com.nuclearpotato.soundrecorder.model

import android.util.Log
import com.squareup.haha.guava.collect.Maps
import java.io.File
import java.io.IOException
import java.util.*

interface RecordingStore {
    /**
     * returns a recording with a different name on success.
     *
     * @throws IllegalArgumentException name invalid
     * @throws FileAlreadyExistsException name clash
     * @throws FileSystemException general failure
     */
    fun rename(recording: Recording, name: String): Recording
    /**
     * @throws NoSuchFileException file not found
     * @throws FileSystemException general failure
     */
    fun delete(recording: Recording)

    /**
     * Creates a new recording file with a generic that can be used as a recording target.
     */
    fun create(): Recording

    /**
     * Set up the recordings dir, if needed
     */
    fun setup(): Boolean

    fun file(recording: Recording): File

    fun listRecordings(): List<Recording>
}

object RecordingUtils {
    const val EXTENSION = ".mp4"
    private val INVALID_CHARS = charArrayOf('\\', '|', '?', '*', '<', '>', ':', '\'', '\"', '\u0000', '/')

    const val NAME_VALID = 0
    const val NAME_EMPTY = 1
    const val NAME_INVALID = 3
    const val NAME_FLAG_MASK = 0x00FF

    fun isNameValid(name: String): Int{
        if(name.trim().isEmpty()){
            return NAME_EMPTY
        }

        val invalid = firstInvalid(name)

        return if(invalid == null) {
            NAME_VALID
        } else {
            NAME_INVALID or (invalid.toInt() shl 16)
        }
    }

    private fun firstInvalid(name: String): Char? {
        return name.firstOrNull { INVALID_CHARS.contains(it) }
    }

    fun flagOf(result: Int): Int{
        return result and NAME_FLAG_MASK
    }

    fun charOf(result: Int): Char{
        return (result ushr 16).toChar()
    }
}

class RecordingStoreImpl(private val dir: File) : RecordingStore{

    val tag = javaClass.simpleName

    override fun listRecordings(): List<Recording> {
        Log.d(tag, "listing recordings in dir ${dir.absolutePath}")
        return dir
                .listFiles { file, s ->
                    s.endsWith(RecordingUtils.EXTENSION)
                }
                .toList()
                .map {
                    Recording(it.nameWithoutExtension, null)
                }
                .apply {
                    Log.d(tag, joinToString())
                }
    }

    override fun file(recording: Recording): File {
        return fileOf(recording.name)
    }

    override fun rename(recording: Recording, name: String): Recording{
        ensureDirectory()
        if(RecordingUtils.isNameValid(name) != RecordingUtils.NAME_VALID){
            throw IllegalArgumentException()
        }
        val oldFile = fileOf(recording.name)
        val newFile = fileOf(name)

        if(!oldFile.exists()){
            throw NoSuchFileException(oldFile, newFile, "cannot rename, old file not found")
        }

        if(newFile.exists()){
            throw FileAlreadyExistsException(oldFile)
        }

        if(!oldFile.renameTo(newFile)){
            throw FileSystemException(oldFile, newFile, "rename failed")
        }

        return recording.copy(name = name)
    }

    override fun delete(recording: Recording){
        ensureDirectory()
        val file = fileOf(recording.name)
        if(!file.exists()){
            throw NoSuchFileException(file)
        }
        if(!file.delete()){
            throw FileSystemException(file, null, "could not delete file")
        }
    }

    override fun create(): Recording {
        ensureDirectory()
        val name = System.currentTimeMillis().toString()
        val recording = Recording(name, null)
        return recording
    }

    override fun setup(): Boolean {
        try {
            ensureDirectory()
            return true
        } catch (e: IOException) {
            return false
        }
    }

    fun getTargetFile(recording: Recording): File {
        return fileOf(recording.name)
    }

    private fun ensureDirectory() {
        if(!dir.exists() && !dir.mkdirs()){
            throw IOException("cannot create recordings dir at " + dir.absolutePath)
        }
        val nomedia = File(dir, ".nomedia")
        if(!nomedia.exists() && !nomedia.createNewFile()) {
            throw IOException("could not create .nomedia")
        }
    }

    private fun fileOf(name: String) = File(dir, name + RecordingUtils.EXTENSION)
}

class RecordingStoreStub : RecordingStore {

    override fun listRecordings(): List<Recording> {
        return recordings.toList().map { it.second }
    }

    override fun file(recording: Recording): File {
        throw UnsupportedOperationException("not supported by stub")
    }

    // LinkedHashMap keeps insertion order, that works fine for now.
    val recordings = LinkedHashMap<String, Recording>()

    override fun rename(recording: Recording, name: String): Recording {
        if(!recordings.containsKey(recording.name)){
            throw NoSuchFileException(fileOf(recording.name))
        }
        if(recordings.containsKey(name)){
            throw FileAlreadyExistsException(fileOf(name))
        }

        recordings.remove(recording.name)
        val renamed = recording.copy(name = name)
        recordings.put(name, renamed)
        return renamed
    }

    override fun delete(recording: Recording) {
        if (recordings.containsKey(recording.name)){
            recordings.remove(recording.name)
        } else {
            throw NoSuchFileException(fileOf(recording.name))
        }
    }

    override fun create(): Recording {
        val recording = Recording(System.currentTimeMillis().toString(), null)
        recordings.put(recording.name, recording)
        return recording
    }

    override fun setup(): Boolean {
        return true
    }

    private fun fileOf(name: String) = File(name)

    fun add(recording: Recording): Recording{
        recordings.put(recording.name, recording)
        return recording
    }
}
