package com.nuclearpotato.soundrecorder.save

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AlertDialog
import com.nuclearpotato.soundrecorder.R
import com.nuclearpotato.soundrecorder.controls.BaseSaveDialog
import com.nuclearpotato.soundrecorder.injection.Components
import com.nuclearpotato.soundrecorder.model.Recording
import com.nuclearpotato.soundrecorder.model.RecordingParcel
import com.nuclearpotato.soundrecorder.model.RecordingStore
import javax.inject.Inject

class SaveDialog: BaseSaveDialog(), SaveDialogView {

    private lateinit var presenter: SaveDialogPresenter

    @Inject
    internal lateinit var store: RecordingStore

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Components.soundRecorderComponent(activity).inject(this)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): AlertDialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        presenter = SaveDialogPresenter(context, store, recording()!!, this)
        return dialog
    }

    override fun onPositiveClick() {
        presenter.onSaveClick()
    }

    override fun onNegativeClick() {
        presenter.onDelete()
    }

    companion object {
        fun newInstance(context: Context, recording: Recording): SaveDialog {
            val fragment = SaveDialog()
            val bundle = Bundle()
            bundle.putString(ARG_TITLE, context.getString(R.string.save_dialog_title))
            bundle.putParcelable(ARG_RECORDING, RecordingParcel(recording))
            bundle.putString(ARG_POSITIVE_TEXT, context.getString(R.string.save_dialog_save))
            bundle.putString(ARG_NEGATIVE_TEXT, context.getString(R.string.save_dialog_discard))
            fragment.arguments = bundle
            return fragment
        }
    }
}