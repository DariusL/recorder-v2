package com.nuclearpotato.soundrecorder.save

import android.content.Context
import com.nuclearpotato.soundrecorder.R
import com.nuclearpotato.soundrecorder.model.Duration
import com.nuclearpotato.soundrecorder.model.Recording
import com.nuclearpotato.soundrecorder.model.RecordingStore
import com.nuclearpotato.soundrecorder.model.RecordingUtils

open class BaseSaveDialogPresenter(
        private val context: Context,
        recording: Recording,
        private val view: SaveDialogView) {

    init {
        view.setDuration(recording.duration ?: Duration(0))
        view.name = recording.name
        view.nameChanges().subscribe { onNameChanged(it) }
    }

    private fun onNameChanged(newName: String) {
        val result = RecordingUtils.isNameValid(newName)

        when (RecordingUtils.flagOf(result)) {
            RecordingUtils.NAME_EMPTY -> {
                view.setError(context.getString(R.string.dialog_error_empty))
            }
            RecordingUtils.NAME_INVALID -> {
                val char = RecordingUtils.charOf(result)
                view.setError(String.format(context.getString(R.string.dialog_error_illegal), char))
            }
            else -> {
                view.setError("")
            }

        }

        view.enablePositiveButton(result == RecordingUtils.NAME_VALID)
    }
}
