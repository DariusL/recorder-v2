package com.nuclearpotato.soundrecorder.save

import com.nuclearpotato.soundrecorder.model.Duration
import rx.Observable

interface SaveDialogView {
    fun setDuration(duration: Duration)
    var name: String
    fun setError(error: String?)
    fun enablePositiveButton(enable: Boolean)
    fun nameChanges(): Observable<String>
}
