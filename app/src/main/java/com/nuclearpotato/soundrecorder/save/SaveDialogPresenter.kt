package com.nuclearpotato.soundrecorder.save

import android.content.Context
import com.nuclearpotato.soundrecorder.model.Recording
import com.nuclearpotato.soundrecorder.model.RecordingStore

class SaveDialogPresenter(
        context: Context,
        private val store: RecordingStore,
        private val recording: Recording,
        private val view: SaveDialogView): BaseSaveDialogPresenter(context, recording, view) {

    fun onSaveClick() {
        try {
            store.rename(recording, view.name)
        } catch(e: IllegalArgumentException){
            // ignore, error is already visible
        } catch(e: FileAlreadyExistsException){
            // TODO error
        } catch(e: FileSystemException){
            // TODO error
        }
    }

    fun onDelete() {
        store.delete(recording)
    }
}
