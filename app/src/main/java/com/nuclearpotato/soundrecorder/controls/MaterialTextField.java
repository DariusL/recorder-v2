package com.nuclearpotato.soundrecorder.controls;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.StringRes;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jakewharton.rxbinding.widget.RxTextView;
import com.jakewharton.rxbinding.widget.TextViewAfterTextChangeEvent;
import com.nuclearpotato.soundrecorder.R;

import rx.Observable;
import rx.functions.Func1;

public class MaterialTextField extends LinearLayout {
    private static final int[] THEME_ATTRS = {R.attr.colorAccent};

    protected TextView label;
    protected TextView error;
    protected EditText field;
    private CharSequence hint;

    private int accentColor;
    private int hintColor;

    public MaterialTextField(Context context) {
        this(context, null);
    }

    public MaterialTextField(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MaterialTextField(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setup(context, attrs);
    }

    @TargetApi(21)
    public MaterialTextField(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        setup(context, attrs);
    }

    protected void setup(Context context, AttributeSet attrs){
        setOrientation(VERTICAL);
        inflate(context, R.layout.view_text_field, this);
        final TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.MaterialTextField);

        label = (TextView) findViewById(R.id.material_field_label);
        error = (TextView) findViewById(R.id.material_field_error);
        field = (EditText) findViewById(R.id.material_field);

        hint = a.getString(R.styleable.MaterialTextField_android_hint);
        error.setText(a.getString(R.styleable.MaterialTextField_mtf_error));
        field.setHint(hint);
        label.setText(hint);

        loadColorsFromTheme(context);

        int imeOptions = a.getInt(R.styleable.MaterialTextField_android_imeOptions, EditorInfo.IME_ACTION_NONE);
        int type = a.getInt(R.styleable.MaterialTextField_android_inputType, InputType.TYPE_CLASS_TEXT);

        a.recycle();

        field.setInputType(type);
        field.setImeOptions(imeOptions);
        label.setText(field.getHint());

        field.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                updateHint();
            }
        });

        field.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                updateHint();
            }
        });
        updateHint();
    }

    private void loadColorsFromTheme(Context context){
        TypedValue accentValue = new TypedValue();
        context.getTheme().resolveAttribute(android.support.v7.appcompat.R.attr.colorAccent, accentValue, true);
        TypedArray a = context.obtainStyledAttributes(accentValue.data, new int[]{android.support.v7.appcompat.R.attr.colorAccent});
        accentColor = a.getColor(0, 0);
        hintColor = label.getCurrentTextColor();
        a.recycle();
    }

    public void setText(String text){
        field.setText(text);
        updateHint();
    }

    public Observable<String> afterTextChange() {
        return RxTextView.afterTextChangeEvents(field)
                .map(new Func1<TextViewAfterTextChangeEvent, String>() {
                    @Override
                    public String call(TextViewAfterTextChangeEvent textViewAfterTextChangeEvent) {
                        return textViewAfterTextChangeEvent.editable().toString();
                    }
                });
    }

    public String getText(){
        return field.getText().toString();
    }

    public void setError(String error){
        this.error.setText(error);
    }

    public void setError(@StringRes int error){
        setError(getContext().getString(error));
    }

    public boolean isEmpty(){
        return field.getText().length() == 0;
    }

    private void updateHint(){
        label.setActivated(field.isFocused());
        label.setTextColor(field.isFocused() ? accentColor : hintColor);
        boolean hasText = field.getText().length() != 0;
        if(hasText || field.isFocused()){
            label.setVisibility(VISIBLE);
            field.setHint(null);
        }else{
            label.setVisibility(INVISIBLE);
            field.setHint(hint);
        }
    }

    protected static boolean isEmpty(String str){
        return str == null || str.trim().isEmpty();
    }

    public EditText getEditText(){
        return field;
    }
}