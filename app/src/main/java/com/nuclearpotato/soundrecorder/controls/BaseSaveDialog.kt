package com.nuclearpotato.soundrecorder.controls

import android.content.DialogInterface
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import android.text.Editable
import android.text.TextWatcher
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.jakewharton.rxbinding.widget.RxTextView
import com.nuclearpotato.soundrecorder.R
import com.nuclearpotato.soundrecorder.model.Duration
import com.nuclearpotato.soundrecorder.model.Recording
import com.nuclearpotato.soundrecorder.model.RecordingParcel

abstract class BaseSaveDialog : DialogFragment() {

    @BindView(R.id.text_length)
    internal lateinit var length: TextView
    @BindView(R.id.field_name)
    internal lateinit var nameField: MaterialTextField

    override fun onCreateDialog(savedInstanceState: Bundle?): AlertDialog {
        @SuppressWarnings("all")
        val content = activity.layoutInflater.inflate(R.layout.dialog_layout, null)
        ButterKnife.bind(this, content)

        val builder = AlertDialog.Builder(activity)
        builder.setTitle(arguments.getString(ARG_TITLE))
                .setCancelable(false)
                .setView(content)
                .setPositiveButton(arguments.getString(ARG_POSITIVE_TEXT)) { dialog, which ->
                    onPositiveClick() }
                .setNegativeButton(arguments.getString(ARG_NEGATIVE_TEXT)) { dialog, which ->
                    onNegativeClick() }

        val dialog = builder.create()

        return dialog
    }

    // we don't need the initial emission, messes up enablePositiveButton
    fun nameChanges() = nameField.afterTextChange().skip(1)

    protected abstract fun onPositiveClick();

    protected abstract fun onNegativeClick();

    var name: String
        get() = nameField.text
        set(value) {
            nameField.text = value
        }

    fun setError(error: String?) {
        nameField.setError(error)
    }

    fun setDuration(duration: Duration) {
        length.text = formatDuration(duration)
    }

    fun enablePositiveButton(enable: Boolean) {
        (dialog as AlertDialog).getButton(DialogInterface.BUTTON_POSITIVE).isEnabled = enable
    }

    private fun formatDuration(duration: Duration) = "${duration.getHours()}:${duration.getMinutes()}:${duration.getSeconds()}"

    protected fun recording(): Recording? = arguments.getParcelable<RecordingParcel>(ARG_RECORDING).data

    companion object {
        const val ARG_TITLE = "SaveDialog.arg.title"
        const val ARG_RECORDING = "SaveDialog.arg.recording"
        const val ARG_POSITIVE_TEXT = "SaveDialog.arg.positive"
        const val ARG_NEGATIVE_TEXT = "SaveDialog.arg.negative"

        fun arguments(recording: Recording): Bundle {
            return Bundle()
                    .apply {
                        putParcelable(ARG_RECORDING, RecordingParcel(recording))
                    }
        }
    }
}
