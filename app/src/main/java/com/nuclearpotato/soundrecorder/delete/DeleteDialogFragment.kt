package com.nuclearpotato.soundrecorder.delete

import android.app.Dialog
import android.os.Bundle
import android.support.annotation.StringRes
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import com.nuclearpotato.soundrecorder.R
import com.nuclearpotato.soundrecorder.model.Recording
import com.nuclearpotato.soundrecorder.model.RecordingParcel
import com.nuclearpotato.soundrecorder.toArrayList
import java.util.*

class DeleteDialogFragment: DialogFragment() {


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val callbacks = activity as Callbacks
        val recordings = arguments
                .getParcelableArrayList<RecordingParcel>(ARG_RECORDINGS)
                .map { it.data }
        return AlertDialog.Builder(activity)
                .setTitle(getTitle(recordings))
                .setMessage(generateBody(recordings))
                .setNegativeButton(R.string.cancel, null)
                .setPositiveButton(R.string.delete_dialog_button_delete, { foo, bar -> callbacks.onDeleteConfirm() })
                .create()
    }

    @StringRes
    private fun getTitle(recordings: List<Recording>): Int {
        return if (recordings.size == 1)
            R.string.delete_dialog_title_single
        else
            R.string.delete_dialog_title_multiple
    }

    companion object {
        const val ARG_RECORDINGS = "DeleteDialogFragment.arg.recordings"

        fun generateBody(recordings: List<Recording>): String {
            return when (recordings.size) {
                1 -> recordings.single().name
                else -> recordings
                        .mapIndexed { i, recording -> "${i + 1}. ${recording.name}" }
                        .joinToString("\n")
            }
        }

        fun arguments(recordings: List<Recording>): Bundle {
            return Bundle()
                    .apply {
                        putParcelableArrayList(ARG_RECORDINGS, recordings.map(::RecordingParcel).toArrayList())
                    }
        }
    }

    interface Callbacks {
        fun onDeleteConfirm()
    }
}