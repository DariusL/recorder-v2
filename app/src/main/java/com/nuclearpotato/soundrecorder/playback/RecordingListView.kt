package com.nuclearpotato.soundrecorder.playback

import com.nuclearpotato.soundrecorder.model.Recording

interface RecordingListView {
    fun setRecordings(recordings: List<RecordingViewModel>)
    val player: Player
    fun setLoading(loading: Boolean)
    fun enableActionMode(): ActionMode
    fun showDeleteDialog(recordings: List<Recording>)
    fun showRenameDialog(recording: Recording)

    interface Player {
        fun play(recording: Recording)
        fun stop()
    }

    interface ActionMode {
        fun disable()
        var count: Int
    }

}