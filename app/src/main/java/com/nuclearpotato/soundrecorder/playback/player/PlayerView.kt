package com.nuclearpotato.soundrecorder.playback.player

import com.nuclearpotato.soundrecorder.model.Duration
import com.nuclearpotato.soundrecorder.model.Recording
import rx.Observable

interface PlayerView {
    fun setRecording(recording: Recording)
    fun setCurrentTime(duration: Duration)
    fun setCurrentProgress(progress: Float)
    fun setPlaying(playing: Boolean)

    val playClicks: Observable<Any>
    val seeks: Observable<Float>
}