package com.nuclearpotato.soundrecorder.playback

import com.nuclearpotato.soundrecorder.model.Recording

data class RecordingViewModel(
        val recording: Recording,
        val playing: Boolean,
        val selected: Boolean)