package com.nuclearpotato.soundrecorder.playback

import com.nuclearpotato.soundrecorder.model.Recording
import com.nuclearpotato.soundrecorder.model.RecordingStore
import rx.Observable
import rx.Scheduler

class RecordingListPresenter(
        private val recordingStore: RecordingStore,
        reader: RecordingReader,
        workerScheduler: Scheduler,
        mainThreadScheduler: Scheduler,
        private val view: RecordingListView) {

    private lateinit var recordings: MutableList<RecordingViewModel>
    private var actionMode: RecordingListView.ActionMode? = null

    init {
        view.setLoading(true)
        Observable
                .defer { Observable.from(recordingStore.listRecordings()) }
                .map { RecordingViewModel(recording = reader.read(it), playing = false, selected = false) }
                .toList()
                .subscribeOn(workerScheduler)
                .observeOn(mainThreadScheduler)
                .subscribe {
                    recordings = it
                    view.setRecordings(it)
                    view.setLoading(false)
                }
    }

    fun onRecordingPress(position: Int) {
        val recording = recordings[position]
        if (actionMode != null) {
            recordings[position] = recording.copy(selected = !recording.selected)
            val count = recordings.count { it.selected }
            actionMode!!.count = count
            if (count == 0) {
                finishActionMode()
            }
        } else {
            view.player.play(recording.recording)
            recordings.replaceIndexed { i, recordingViewModel -> recordingViewModel.copy(playing = i == position) }
        }
        updateRecordings()
    }

    fun onSelectionPress() {
        actionMode = enableActionMode()
    }

    fun onActionModeDestroyed() {
        actionMode = null
        clearSelection()
        updateRecordings()
    }

    private fun clearSelection() {
        recordings.replace { it -> it.copy(selected = false) }
    }

    private fun finishActionMode() {
        actionMode!!.disable()
    }

    fun onRecordingLongPress(position: Int) {
        if (actionMode == null){
            actionMode = enableActionMode()
        }
        onRecordingPress(position)
    }

    private fun enableActionMode() = view.enableActionMode()

    fun onDeletePress() {
        verifyInActionMode("Can only delete in action mode")
        val toDelete = recordings
                .filter { it.selected }
                .map { it.recording }
        if(toDelete.count() < 1) {
            throw IllegalStateException("Cannot delete with zero items")
        }
        view.showDeleteDialog(toDelete)
    }

    fun onDeleteConfirm() {
        verifyInActionMode("Can only delete in action mode")
        if (recordings.any { it.playing && it.selected }) {
            view.player.stop()
        }
        recordings
                .filter { it.selected }
                .forEach { recordingStore.delete(it.recording) }
        recordings.removeAll { it.selected }
        updateRecordings()
        finishActionMode()
    }

    fun onRenamePress() {
        verifyInActionMode("Can only rename in action mode")
        val recording = recordings
                .single { it.selected }
                .let { it.recording }
        view.showRenameDialog(recording)
    }

    private fun verifyInActionMode(message: String) {
        if (actionMode == null) {
            throw IllegalStateException(message)
        }
    }

    fun onRenameConfirm(name: String) {
        val recording = recordings
                .single { it.selected }
                .let { it.recording }
        val renamed = recordingStore.rename(recording, name)
        recordings.replace { if (it.selected) it.copy(recording = renamed) else it }
        updateRecordings()
    }

    private fun updateRecordings() {
        view.setRecordings(recordings)
    }

    fun onSelectAllPress() {
        verifyInActionMode("Cannot select all out of action mode")
        recordings.replace { it.copy(selected = true) }
        updateRecordings()
        actionMode!!.count = recordings.count { it.selected }
    }

    fun getSelection(): List<Recording> {
        return recordings
                .filter { it.selected }
                .map { it.recording }
    }
}

inline fun <T> MutableList<T>.replace(function: (T) -> T) {
    for (i in indices) {
        this[i] = function(this[i])
    }
}

inline fun <T> MutableList<T>.replaceIndexed(function: (Int, T) -> T) {
    for (i in indices) {
        this[i] = function(i, this[i])
    }
}