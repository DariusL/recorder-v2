package com.nuclearpotato.soundrecorder.playback

import android.media.AudioManager
import android.media.MediaPlayer
import com.nuclearpotato.soundrecorder.model.Duration
import com.nuclearpotato.soundrecorder.model.Recording
import com.nuclearpotato.soundrecorder.model.RecordingStore
import java.io.Closeable

interface RecordingReader {
    /**
     * Synchronously loads the recording and returns a [Recording] with a parsed
     * duration, or null duration on failure.
     */
    fun read(recording: Recording): Recording
}

class RecordingreaderImpl(private val store: RecordingStore) : RecordingReader, Closeable {

    var player: MediaPlayer? = MediaPlayer()

    @Synchronized
    override fun read(recording: Recording): Recording {
        return read(player!!, recording)
    }

    private fun read(player: MediaPlayer, recording: Recording): Recording {
        try {
            player.reset()
            player.setAudioStreamType(AudioManager.STREAM_MUSIC)
            player.setDataSource(store.file(recording).absolutePath)
            player.prepare()
            return recording.copy(duration = Duration(player.duration.toLong()))
        } catch (e: Exception) {
            e.printStackTrace()
            return recording
        }
    }

    @Synchronized
    override fun close() {
        player?.release()
        player = null
    }
}