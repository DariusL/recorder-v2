package com.nuclearpotato.soundrecorder.playback

import android.app.ProgressDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.view.ActionMode
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import butterknife.BindView
import butterknife.ButterKnife
import com.nuclearpotato.soundrecorder.R
import com.nuclearpotato.soundrecorder.delete.DeleteDialogFragment
import com.nuclearpotato.soundrecorder.injection.Components
import com.nuclearpotato.soundrecorder.injection.IO_SCHEDULER
import com.nuclearpotato.soundrecorder.injection.MAIN_SCHEDULER
import com.nuclearpotato.soundrecorder.model.Recording
import com.nuclearpotato.soundrecorder.model.RecordingStore
import com.nuclearpotato.soundrecorder.rename.RenameDialogFragment
import com.nuclearpotato.soundrecorder.toArrayList
import rx.Scheduler
import javax.inject.Inject
import javax.inject.Named

class RecordingsListActivity : AppCompatActivity(), RecordingListView, DeleteDialogFragment.Callbacks,
        RenameDialogFragment.Callback {
    override val player = MockPlayer()

    @Inject
    internal lateinit var store: RecordingStore
    @Inject
    internal lateinit var recordingReader: RecordingReader
    @field:[Inject Named(IO_SCHEDULER)]
    internal lateinit var workerScheduler: Scheduler
    @field:[Inject Named(MAIN_SCHEDULER)]
    internal lateinit var mainScheduler: Scheduler

    private var loadingDialog: ProgressDialog? = null

    private lateinit var presenter: RecordingListPresenter
    private lateinit var adapter: RecordingsAdapter

    @BindView(R.id.recordings_list_list)
    internal lateinit var recycler: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Components.soundRecorderComponent(this).inject(this)

        supportActionBar!!.apply {
            setHomeButtonEnabled(true)
            setDisplayHomeAsUpEnabled(true)
            setTitle(R.string.activity_recordings)
        }

        setContentView(R.layout.recordings_list)
        ButterKnife.bind(this)

        recycler.layoutManager = LinearLayoutManager(this)
        this.adapter = RecordingsAdapter(this, itemClickListener, itemLongClickListener)
        recycler.adapter = adapter

        presenter = RecordingListPresenter(store, recordingReader, workerScheduler, mainScheduler, this)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        return super.onCreateOptionsMenu(menu)
    }

    private val actionModeCallback = object : ActionMode.Callback {
        override fun onPrepareActionMode(mode: ActionMode?, menu: Menu?): Boolean {
            return false
        }

        override fun onActionItemClicked(mode: ActionMode?, item: MenuItem?): Boolean {
            return this@RecordingsListActivity.onActionItemClicked(item!!)
        }

        override fun onCreateActionMode(mode: ActionMode?, menu: Menu?): Boolean {
            menuInflater.inflate(R.menu.recordings_cab, menu)
            return true
        }

        override fun onDestroyActionMode(mode: ActionMode?) {
            presenter.onActionModeDestroyed()
        }
    }

    private fun onActionItemClicked(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.cab_delete -> {
                presenter.onDeletePress()
                return true
            }
            R.id.cab_edit -> {
                presenter.onRenamePress()
                return true
            }
            R.id.cab_share -> {
                shareSelected()
                return true
            }
            R.id.cab_sel -> {
                presenter.onSelectAllPress()
                return true
            }
            else -> return false
        }
    }

    private fun shareSelected() {
        val uris = presenter.getSelection()
            .map { Uri.fromFile(store.file(it)) }
        val intent = Intent()
        intent.type = "audio/mpeg"
        if (uris.size == 1) {
            intent.action = Intent.ACTION_SEND
            intent.putExtra(Intent.EXTRA_STREAM, uris.single())
        } else {
            intent.action = Intent.ACTION_SEND_MULTIPLE
            intent.putExtra(Intent.EXTRA_STREAM, uris.toArrayList())
        }

        startActivity(intent)
    }

    override fun setRecordings(recordings: List<RecordingViewModel>) {
        adapter.setRecordings(recordings)
    }

    private val itemClickListener = { position: Int ->
        presenter.onRecordingPress(position)
    }

    private val itemLongClickListener = { position: Int ->
        presenter.onRecordingLongPress(position)
    }

    override fun setLoading(loading: Boolean) {
        if (loading) {
            loadingDialog = ProgressDialog.show(this, null, "LOADING TEMP TEXT", true, false)
        } else {
            loadingDialog?.dismiss()
            loadingDialog = null
        }
    }

    override fun enableActionMode(): RecordingListView.ActionMode {
        val actionMode = startSupportActionMode(actionModeCallback)!!
        return ActionModeWrapper(actionMode)
    }

    private class ActionModeWrapper(private val actionMode: ActionMode) : RecordingListView.ActionMode {

        override fun disable() {
            actionMode.finish()
        }

        override var count: Int = 0
            get() = field
            set(value) {
                field = value
                actionMode.title = value.toString() + " ITEMS SELECTED"
                enableRename(value == 1)
            }

        private fun enableRename(enable: Boolean) {
            actionMode.menu.findItem(R.id.cab_edit).isVisible = enable
        }
    }

    override fun showDeleteDialog(recordings: List<Recording>) {
        DeleteDialogFragment()
                .apply {
                    arguments = DeleteDialogFragment.arguments(recordings)
                    show(supportFragmentManager, null)
                }
    }

    override fun showRenameDialog(recording: Recording) {
        RenameDialogFragment()
                .apply {
                    arguments = RenameDialogFragment.arguments(this@RecordingsListActivity, recording)
                    show(supportFragmentManager, null)
                }
    }

    inner class MockPlayer : RecordingListView.Player {
        override fun play(recording: Recording) {
            toast("playing " + recording)
        }

        override fun stop() {
            toast("stopped")
        }
    }

    private fun toast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun onDeleteConfirm() {
        presenter.onDeleteConfirm()
    }

    override fun onConfirm(name: String) {
        presenter.onRenameConfirm(name)
    }
}

