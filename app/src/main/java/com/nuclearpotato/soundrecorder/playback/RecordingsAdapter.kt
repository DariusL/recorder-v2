package com.nuclearpotato.soundrecorder.playback

import android.content.Context
import android.support.annotation.ColorInt
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.nuclearpotato.soundrecorder.R
import com.nuclearpotato.soundrecorder.Util

class RecordingsAdapter(
        context: Context,
        val itemClickListener: (Int) -> Unit,
        val itemLongClickListener: (Int) -> Unit) : RecyclerView.Adapter<RecordingsAdapter.RecordingHolder>() {
    private val inflater = LayoutInflater.from(context)

    private var recordings: List<RecordingViewModel> = emptyList()

    fun setRecordings(recordings: List<RecordingViewModel>) {
        this.recordings = recordings
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: RecordingHolder, position: Int) {
        holder.bind(recordings[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecordingHolder {
        return RecordingHolder(inflater.inflate(R.layout.recordings_list_item, parent, false))
    }

    override fun getItemCount(): Int = recordings.count()

    inner class RecordingHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        @BindView(R.id.title_text)
        internal lateinit var name: TextView

        @BindView(R.id.subtitle_text)
        internal lateinit var duration: TextView

        val clickListener = { view: View ->
            itemClickListener.invoke(position(view))
        }

        val longClickListener = { view: View ->
            itemLongClickListener(position(view))
            true
        }

        init {
            ButterKnife.bind(this, itemView)
            itemView.tag = this
            itemView.setOnClickListener(clickListener)
            itemView.setOnLongClickListener(longClickListener)
        }

        fun bind(recordingViewModel: RecordingViewModel) {
            name.text = recordingViewModel.recording.name
            duration.text = recordingViewModel.recording.duration?.toString()
            itemView.setBackgroundColor(getItemColor(recordingViewModel))
        }

        @ColorInt
        private fun getItemColor(model: RecordingViewModel): Int {
            return model.let {
                if (it.selected) {
                    R.color.my_light_blue
                } else if (it.playing) {
                    R.color.my_light_gray
                } else {
                    android.R.color.transparent
                }
            }.let {
                Util.getColor(itemView.context, it)
            }
        }

        private fun position(view: View): Int = (view.tag as RecordingHolder).adapterPosition
    }
}