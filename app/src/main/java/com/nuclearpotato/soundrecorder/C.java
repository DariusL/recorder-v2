package com.nuclearpotato.soundrecorder;

public final class C {
	public static final int RENAME_DIALOG = 0;//id
	public static final int SAMPLING_RATE = 48000;//48KHz sampling rate
	public static final int BIT_RATE = 131072;//128kbps bitrate
	public static final String NEXUS_S = "Nexus S";

	public static final String INVALID_CHARS = "|\\?*<\":>/'\0";
}